<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', 'Client\ArticlesController@browse');

Route::get('/', function () {
    return view('public.homepage');
});

Route::get('/about', function () {
    return view('public.about');
});

Route::get('/contact', function () {
    return view('public.contact');
});

Route::get('/team', function () {
    return view('public.team');
});

Route::get('/clients', function () {
    return view('public.clients');
});
Route::get('/services', function () {
    return view('public.services');
});


Route::get('/career', function () {
    return view('public.career');
});

Route::get('/quote', function () {
    return view('public.quote');
});

Route::get('/testimonial', function () {
    return view('public.testimonial');
});

Route::get('/uniform', function () {
    return view('public.uniform');
});

Route::get('/dealership', function () {
    return view('public.dealership');
});

// Route::get('/testimonial', 'Client\FeedbackController@index');
// Route::get('/feedbacks/create/', 'Client\FeedbackController@create');
// Route::post('/feedbacks/insert/', 'Client\FeedbackController@insert');

// Route::get('/feedbacks', 'Client\FeedbackController@create');
// Route::post('/feedbacks/insert/', 'Client\FeedbackController@insert');

 





Route::get('/requirement', 'Client\RequirementController@index');
Route::post('/requirement/insert', 'Client\RequirementController@insert');

Route::get('/admin/requirement', 'Admin\RequirementController@index');
Route::get('/admin/requirement/delete/{id}', 'Admin\RequirementController@delete');



Route::get('/contact', 'Client\ContactController@index');
Route::post('/contact/insert', 'Client\ContactController@insert');

Route::get('/images/gallery/', 'Client\ImageController@index');
Route::get('/images/gallery/get/{searchval?}', 'Client\ImageController@getImages');

Route::get('/articles/all', 'Client\ArticlesController@browse');
Route::post('/articles/search', 'Client\ArticlesController@search');
Route::get('/articles/show/{id?}', 'Client\ArticlesController@showArticle');
Route::get('/articles/gallery/get/{searchval?}', 'Client\ArticlesController@getArticles');
// Route::get('/videos/gallery/', 'Client\VideoController@index');
// Route::get('/videos/gallery/get/{searchval?}', 'Client\VideoController@getVideos');


Route::get('/aboutme', 'Client\AboutmeController@getAboutme');


Auth::routes();
Route::get('/admin', 'Admin\AdminController@index');
Route::get('/admin/images', 'Admin\ImageController@index');
Route::get('/admin/images/create/', 'Admin\ImageController@create');
Route::post('/admin/images/insert/', 'Admin\ImageController@insert');
Route::get('/admin/images/edit/{id}', 'Admin\ImageController@edit');
Route::post('/admin/images/update/{id}', 'Admin\ImageController@update');
Route::get('/admin/images/delete/{id}', 'Admin\ImageController@delete');



Route::get('/admin/aboutme', 'Admin\AboutmeController@index');
Route::get('/admin/aboutme/create/', 'Admin\AboutmeController@create');
Route::post('/admin/aboutme/insert/', 'Admin\AboutmeController@insert');
Route::get('/admin/aboutme/edit/{id}', 'Admin\AboutmeController@edit');
Route::post('/admin/aboutme/update/{id}', 'Admin\AboutmeController@update');
Route::get('/admin/aboutme/delete/{id}', 'Admin\AboutmeController@delete');


Route::get('/admin/product', 'Admin\UserimagesController@index');
Route::get('/admin/product/create/', 'Admin\UserimagesController@create');
Route::post('/admin/product/insert/', 'Admin\UserimagesController@insert');
Route::get('/admin/product/edit/{id}', 'Admin\UserimagesController@edit');
Route::post('/admin/product/update/{id}', 'Admin\UserimagesController@update');
Route::get('/admin/product/delete/{id}', 'Admin\UserimagesController@delete');

// Route::get('/admin/videos', 'Admin\VideoController@index');
// Route::get('/admin/videos/create/', 'Admin\VideoController@create');
// Route::post('/admin/videos/insert/', 'Admin\VideoController@insert');
// Route::get('/admin/videos/edit/{id}', 'Admin\VideoController@edit');
// Route::post('/admin/videos/update/{id}', 'Admin\VideoController@update');
// Route::get('/admin/videos/delete/{id}', 'Admin\VideoController@delete');



Route::get('/homepage', 'Client\HomepageController@index');
Route::post('/homepage/insert', 'Client\HomepageController@insert');

Route::get('/admin/homepage', 'Admin\HomepageController@index');
Route::get('/admin/homepage/delete/{id}', 'Admin\HomepageController@delete');

Route::get('/admin/contact', 'Admin\ContactController@index');
Route::get('/admin/contact/delete/{id}', 'Admin\ContactController@delete');

//packages
Route::get('/admin/userblogs', 'Admin\UserblogsController@index');
Route::get('/admin/userblogs/create/', 'Admin\UserblogsController@create');
Route::post('/admin/userblogs/insert/', 'Admin\UserblogsController@insert');
Route::get('/admin/userblogs/edit/{id}', 'Admin\UserblogsController@edit');
Route::post('/admin/userblogs/update/{id}', 'Admin\UserblogsController@update');
Route::get('/admin/userblogs/delete/{id}', 'Admin\UserblogsController@delete');


Route::get('/userblogs/all', 'Client\UserblogsController@browse');
Route::post('/userblogs/search', 'Client\UserblogsController@search');
Route::get('/userblogs/show/{id?}', 'Client\UserblogsController@showblog');
Route::get('/userblogs/gallery/get/{searchval?}', 'Client\UserblogsController@getblogs');
Route::get('/userblogs', 'Client\UserblogsController@browse');
Route::post('/order', 'Client\UserblogsController@order');

Route::get('/uniform', 'Client\UniformController@index');
Route::post('/uniform/insert', 'Client\UniformController@insert');

Route::get('/admin/uniform', 'Admin\UniformController@index');
Route::get('/admin/uniform/delete/{id}', 'Admin\UniformController@delete');


// feedback
Route::get('/testimonial', 'Client\FeedbackController@index');
Route::get('/feedbacks', 'Client\FeedbackController@create');
Route::post('/feedbacks/insert/', 'Client\FeedbackController@insert');
Route::get('/testimonial', 'Client\FeedbackController@index');
Route::get('/feedbacks/create/', 'Client\FeedbackController@create');
Route::post('/feedbacks/insert/', 'Client\FeedbackController@insert');
Route::get('/admin/feedbacks', 'Admin\FeedbackController@index');
Route::get('/admin/feedbacks/create/', 'Admin\FeedbackController@create');
Route::post('/admin/feedbacks/insert/', 'Admin\FeedbackController@insert');
Route::get('/admin/feedbacks/delete/{id}', 'Admin\FeedbackController@delete');

// video
Route::get('/videos/gallery/', 'Client\VideoController@index');
Route::get('/videos/gallery/get/{searchval?}', 'Client\VideoController@getVideos');

Route::get('/admin/videos', 'Admin\VideoController@index');
Route::get('/admin/videos/create/', 'Admin\VideoController@create');
Route::post('/admin/videos/insert/', 'Admin\VideoController@insert');
Route::get('/admin/videos/edit/{id}', 'Admin\VideoController@edit');
Route::post('/admin/videos/update/{id}', 'Admin\VideoController@update');
Route::get('/admin/videos/delete/{id}', 'Admin\VideoController@delete');
