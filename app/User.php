<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function images()
    {
        return $this->hasMany('App\Image');
    }

	public function userimages()
    {
        return $this->hasMany('App\Userimage');
    }

	public function videos()
    {
        return $this->hasMany('App\Video');
    }
    public function aboutmes()
      {
          return $this->hasMany('App\Aboutme');
      }
      public function blogs()
      {
          return $this->hasMany('App\Userblogs');
      }
}
