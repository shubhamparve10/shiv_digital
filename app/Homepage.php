<?php

namespace App;

use Illuminate\Database\Eloquent\Model;     


	class Homepage extends Model
{


	 public $fillable = [
        'first_name',
        'last_name',
        'email',
       'phone',
       'catg',
       'message',

	 ];

	 public $timestamps = false;

}
