<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Aboutme;

use App\User;
use Hash;

class AboutmeController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */

	 public function index()
	{
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
		$aboutmes = Aboutme::all();
        return view('admin.aboutme', ['aboutmes' => $aboutmes]);
	    }
		else {
		return redirect('/admin');
		}

	}

	public function create()
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
        return view('admin.aboutmeform');
	    }
		else {
		return redirect('/admin');
		}
    }

    public function insert(Request $request)
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
        $aboutme = new Aboutme();
    		$aboutme->caption = $request->input('caption');
				$aboutme->caption1= $request->input('caption1');
				$aboutme->caption2 = $request->input('caption2');
        $file = $request->file('aboutmefile');
				$filename = md5_file($file->path()).'.'.$file->getClientOriginalExtension();
        $file->move(base_path().'/public/images/storage/aboutmes/', $filename);
        $aboutme->path = '/images/storage/aboutmes/'.$filename;
		    $aboutme->rating = 0;
				$request->user()->images()->save($aboutme);
        return redirect('/admin/aboutme/')->with('success', 'Useraboutme has been successfully created');
	}
	else {
	return redirect('/admin');
	}
	}

    public function edit($id)
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
		$aboutme = Aboutme::find($id);
		if(!empty($aboutme))
			return view('admin.aboutmeform', ['aboutme' => $aboutme]);
		else
			return redirect('/admin/aboutme')->with('danger', 'Useraboutme not found');
    }
	else {
	return redirect('/admin');
	}
}

    public function update(Request $request, $id)
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
		$aboutme = Aboutme::find($id);
		if(empty($aboutme))
			return redirect('/admin/aboutme/')->with('danger', 'Useraboutme not found');
			$aboutme->caption = $request->input('caption');
			$aboutme->caption1 = $request->input('caption1');
			$aboutme->caption2 = $request->input('caption2');

		if($request->hasFile('aboutme'))
		{
			$file = $request->file('imagefile');
			$filename = md5_file($file).'.'.$file->getClientOriginalExtension();
			$file->move(base_path().'/public/images/storage/aboutmes/', $filename);
				$aboutme->path = '/public/images/storage/aboutmes/'.$filename;
		}
		$request->user()->images()->save($aboutme);
		return redirect('/admin/aboutme')->with('success', 'Aboutme has been successfully Updated');
    }
	else {
	return redirect('/admin');
	}
	}

    public function delete($id)
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
		$aboutme = Aboutme::find($id);
		$aboutme->delete();
		return redirect('/admin/aboutme/')->with('success', 'aboutme has been successfully Deleted');
        }
		else {
		return redirect('/admin');
		}

   }
}
