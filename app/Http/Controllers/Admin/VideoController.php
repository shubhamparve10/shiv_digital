<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\Tag;
use Auth;
use App\User;
use Hash;

class VideoController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */


    public function index()
    {
        if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        $videos = Video::all();
        return view('admin.videos', ['videos' => $videos]);
	    }
		else {
		return redirect('/admin');
		}
	}

    public function create()
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        return view('admin.videosform');
	    }
		else {
		return redirect('/admin');
		}
	}

    public function insert(Request $request)
    {
        if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
	   	    $video = new Video();
            $video->title = $request->input('title');
            $video->caption = $request->input('caption');
            $video->path = $request->input('video');
		    $video->rating = 0;
			$tagsselect = $request->input('tagsselect');
			$request->user()->videos()->save($video);
            $tagslist = array();
		    foreach ($tagsselect as $tagrequest)
			        {
			            $tag = Tag::where('tag', $tagrequest)->first();
			            if($tag === null)
			            {
			                $newtag = new Tag();
			                $newtag->tag = $tagrequest;
			                $newtag->save();
			                array_push($tagslist, $newtag->id);
			            }
			                else
			            {
			                array_push($tagslist, $tag->id);
			            }
			        }

            $video->tags()->attach($tagslist);
            $request->user()->videos()->save($video);
            return redirect('/admin/videos')->with('success', 'Video has been successfully created');

         }
		 else {
		 return redirect('/admin');
		 }
}



    public function edit($id)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        $video = Video::find($id);
        if(!empty($video))
            return view('admin.videosform', ['video' => $video]);
        else
            return redirect('/admin/videos')->with('danger', 'Video not found');
       }
	   else {
	   return redirect('/admin');
	   }

	}

    public function update(Request $request, $id)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        $video = Video::find($id);
        if(empty($video))
            return redirect('/admin/videos')->with('danger', 'Video not found');

        $video->title = $request->input('title');
        $video->caption = $request->input('caption');
        $video->path = $request->input('video');

        $video->tags()->detach();
        $tagsselect = $request->input('tagsselect');
        $request->user()->videos()->save($video);

        $tagslist = array();

        foreach ($tagsselect as $tagrequest)
        {
            $tag = Tag::where('tag', $tagrequest)->first();
            if($tag === null)
            {
                $newtag = new Tag();
                $newtag->tag = $tagrequest;
                $newtag->save();
                array_push($tagslist, $newtag->id);
            }
            else
            {
                array_push($tagslist, $tag->id);
            }
		}

        $video->tags()->attach($tagslist);
        return redirect('/admin/videos')->with('success', 'Video has been successfully Updated');

	}
     else {
	  return redirect('/admin');
     }
 }


    public function delete($id)
    {
        if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))		{
        $video = Video::find($id);
        $video->tags()->detach();
        $video->delete();
        return redirect('/admin/videos')->with('success', 'Article has been successfully Deleted');
       }
	   else {
	   return redirect('/admin');
	   }
   }
}
