<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Feedback;
use App\User;
use Hash;

class FeedbackController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */


	public function index()
	{
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
		$feedbacks = Feedback::all();
        return view('admin.feedbacks', ['feedbacks' => $feedbacks]);
	    }
		else {
		return redirect('/admin');
		}
	}

	public function create()
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        return view('admin.feedbackform');
	    }

		else {
		return redirect('/admin');
		}
	}




//     public function edit($id)
//     {
// 		if(Auth::check() && (Auth::user()->admin == '1'))
// 		{
// 		$image = Image::find($id);
// 		if(!empty($image))
// 			return view('admin.imagesform', ['image' => $image]);
// 		else
// 			return redirect('/admin/images')->with('danger', 'Image not found');
//         }
// 	else {
// 	return redirect('/admin');
// 	}
//
// }
//
//     public function update(Request $request, $id)
//     {
// 		if(Auth::check() && (Auth::user()->admin == '1'))
// 		{
// 		$image = Image::find($id);
// 		if(empty($image))
// 			return redirect('/admin/images')->with('danger', 'Image not found');
//
// 		$image->title = $request->input('title');
// 		$image->caption = $request->input('caption');
//
// 		if($request->hasFile('image'))
// 		{
// 			$file = $request->file('imagefile');
// 			$filename = md5_file($file).'.'.$file->getClientOriginalExtension();
// 			$file->move(base_path().'/public/images/storage/images/', $filename);
//
// 			$image->path = '/images/storage/images/'.$filename;
// 		}
//
// 		$image->tags()->detach();
// 		$tagsselect = $request->input('tagsselect');
// 		$request->user()->images()->save($image);
//
// 		$tagslist = array();
//
// 		foreach ($tagsselect as $tagrequest)
// 		{
// 			$tag = Tag::where('tag', $tagrequest)->first();
// 			if($tag === null)
// 			{
// 				$newtag = new Tag();
// 				$newtag->tag = $tagrequest;
// 				$newtag->save();
// 				array_push($tagslist, $newtag->id);
// 			}
// 			else
// 			{
// 				array_push($tagslist, $tag->id);
// 			}
// 		}
// 		$image->tags()->attach($tagslist);
// 		return redirect('/admin/images')->with('success', 'Image has been successfully Updated');
//     }
//
// 	else {
// 	return redirect('/admin');
// 	}
// }

    public function delete($id)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
		$feedback = Feedback::find($id);
		$feedback->delete();
		return redirect('/admin/feedbacks')->with('success', 'feedback has been successfully Deleted');
	    }
		else {
		return redirect('/admin');
		}
	}
}
