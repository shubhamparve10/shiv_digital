<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Contact;
use App\User;
use Hash;

class ContactController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */

	 public function index()
	{
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
		$contacts = Contact::all();
        return view('admin.contact', ['contacts' => $contacts]);
	    }
		else {
		return redirect('/admin/contact');
		}

	}




    public function delete($id)
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
		$contact = Contact::find($id);

		$contact->delete();
		return redirect('/admin/contact/')->with('success', 'Contacts has been successfully Deleted');
        }
		else {
		return redirect('/admin');
		}

   }
}
