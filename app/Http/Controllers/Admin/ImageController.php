<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Image;
use App\Tag;
use App\User;
use Hash;

class ImageController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */


	public function index()
	{
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
		$images = Image::all();
        return view('admin.images', ['images' => $images]);
	    }
		else {
		return redirect('/admin');
		}
	}

	public function create()
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        return view('admin.imagesform');
	    }

		else {
		return redirect('/admin');
		}
	}


    public function insert(Request $request)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
        $image = new Image();
        $image->title = $request->input('title');
        $image->caption = $request->input('caption');

        $file = $request->file('imagefile');

        $filename = md5_file($file->path()).'.'.$file->getClientOriginalExtension();
        $file->move(base_path().'/public/images/storage/images/', $filename);

        $image->path = '/images/storage/images/'.$filename;

	    $image->rating = 0;

        $tagsselect = $request->input('tagsselect');
        $request->user()->images()->save($image);

        $tagslist = array();

        // foreach ($tagsselect as $tagrequest)
        // {
        //     $tag = Tag::where('tag', $tagrequest)->first();
        //     if($tag === null)
        //     {
        //         $newtag = new Tag();
        //         $newtag->tag = $tagrequest;
        //         $newtag->save();
        //         array_push($tagslist, $newtag->id);
        //     }
        //         else
        //     {
        //         array_push($tagslist, $tag->id);
        //     }
        // }
        // $image->tags()->attach($tagslist);
        return redirect('/admin/images')->with('success', 'Image has been successfully created');
	}

	else {
	return redirect('/admin');
	}

	}

    public function edit($id)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
		$image = Image::find($id);
		if(!empty($image))
			return view('admin.imagesform', ['image' => $image]);
		else
			return redirect('/admin/images')->with('danger', 'Image not found');
        }
	else {
	return redirect('/admin');
	}

}

    public function update(Request $request, $id)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
		$image = Image::find($id);
		if(empty($image))
			return redirect('/admin/images')->with('danger', 'Image not found');

		$image->title = $request->input('title');
		$image->caption = $request->input('caption');

		if($request->hasFile('image'))
		{
			$file = $request->file('imagefile');
			$filename = md5_file($file).'.'.$file->getClientOriginalExtension();
			$file->move(base_path().'/public/images/storage/images/', $filename);

			$image->path = '/images/storage/images/'.$filename;
		}

		// $image->tags()->detach();
		// $tagsselect = $request->input('tagsselect');
		// $request->user()->images()->save($image);

		// $tagslist = array();

		// foreach ($tagsselect as $tagrequest)
		// {
		// 	$tag = Tag::where('tag', $tagrequest)->first();
		// 	if($tag === null)
		// 	{
		// 		$newtag = new Tag();
		// 		$newtag->tag = $tagrequest;
		// 		$newtag->save();
		// 		array_push($tagslist, $newtag->id);
		// 	}
		// 	else
		// 	{
		// 		array_push($tagslist, $tag->id);
		// 	}
		// }
		// $image->tags()->attach($tagslist);
		return redirect('/admin/images')->with('success', 'Image has been successfully Updated');
    }

	else {
	return redirect('/admin');
	}
}

    public function delete($id)
    {
		if(Auth::check() && (Auth::user()->email == 'admin@gmail.com'))
		{
		$image = Image::find($id);
		$image->tags()->detach();
		$image->delete();
		return redirect('/admin/images')->with('danger', 'Image has been successfully Deleted');
	    }
		else {
		return redirect('/admin');
		}
	}
}
