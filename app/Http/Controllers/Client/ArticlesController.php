<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;

use App\Userimage;

use App\Tag;

use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{

    public function index()
    {
        return view('articles.articles');
    }


    public function getArticles(Request $request, $searchval = 'all')
    {
        if($searchval === 'all')
        {
            $articles = UserImage::paginate(10);
            return view('public.articlesearch', ['results' => $articles]);
        }
        else
        {
            $tag = Tag::where('tag', $searchval)->first();
            $articles = null;
            if($tag)
            {
                $articles = $tag->userimages()->paginate(10);
            }
            return view('articles.articlesearch', ['results' => $articles]);
        }
    }

    public function browse(Request $request)
    {
        $results = UserImage::orderBy('created_at', 'desc')->paginate(10);
        return view('public.articlesearch', ['results' => $results]);
    }

    public function search(Request $request)
    {
        $results = $c = collect();
        if($request->input('searchNavbar') !== null)
        {
            $tag = Tag::where('tag', $request->input('searchNavbar'))->first();
            if(!$tag == null)
            {
                $results = $tag->userimages()->orderBy('created_at', 'desc')->paginate(10);
                session()->put('articleSearchTag', $request->input('searchNavbar'));
            }
            return view('public.articlesearch', ['results' => $results]);
        }
        else
        {
            if(session()->has('articleSearchTag'))
            {
                $val = session('articleSearchTag');
                $results = Tag::where('tag', $val)->first()->articles()->orderBy('created_at', 'desc')->paginate(10);
                return view('public.articlesearch', ['results' => $results]);
            }
        }
    }

    public function showArticle(Request $request, $id)
    {
        $article = UserImage::find($id);
        return view('public.showarticle', ['article' => $article]);
    }


}
