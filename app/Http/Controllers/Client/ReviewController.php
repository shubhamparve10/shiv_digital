<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Review;

use App\User;
use Hash;

class ReviewController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */

	 public function index()
	{
		if(Auth::check())
		{
		$reviews = Review::all();
        return view('admin.reviewAdmin', ['reviews' => $reviews]);
	    }
		else {
		return redirect('/admin');
		}

	}

	public function create()
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
        return view('admin.reviewform');
	    }
		else {
		return redirect('/admin');
		}
    }

    public function insert(Request $request)
    {
		if(Auth::check() && (Auth::user()->admin == '1'))
		{
        $review = new Review();
				$review->name = $request->input('name');
				$review->cnumber = $request->input('cnumber');
				$review->rating = $request->input('rating');
				$review->bdate = $request->input('bdate');
    		$review->adate = $request->input('adate');
				$review->caption1= $request->input('caption');
				$review->caption1= $request->input('caption1');


				$request->save($review);
        return redirect('/admin')->with('success', 'Review has been successfully created');
	}
	else {
	return redirect('/admin');
	}

	}

//     public function edit($id)
//     {
// 		if(Auth::check() && (Auth::user()->admin == '1'))
// 		{
// 		$aboutme = Aboutme::find($id);
// 		if(!empty($aboutme))
// 			return view('admin.aboutmeform', ['aboutme' => $aboutme]);
// 		else
// 			return redirect('/admin/aboutme')->with('danger', 'Useraboutme not found');
//     }
// 	else {
// 	return redirect('/admin');
// 	}
//
// }
//
//     public function update(Request $request, $id)
//     {
// 		if(Auth::check() && (Auth::user()->admin == '1'))
// 		{
// 		$aboutme = Aboutme::find($id);
// 		if(empty($aboutme))
// 			return redirect('/admin/aboutme/')->with('danger', 'Useraboutme not found');
// 			$aboutme->caption = $request->input('caption');
// 			$aboutme->caption1 = $request->input('caption1');
// 			$aboutme->caption2 = $request->input('caption2');
//
// 		if($request->hasFile('aboutme'))
// 		{
// 			$file = $request->file('imagefile');
// 			$filename = md5_file($file).'.'.$file->getClientOriginalExtension();
// 			$file->move(base_path().'/public/images/storage/aboutmes/', $filename);
// 				$aboutme->path = '/public/images/storage/aboutmes/'.$filename;
// 		}
// 		$request->user()->images()->save($aboutme);
// 		return redirect('/admin/aboutme')->with('success', 'Aboutme has been successfully Updated');
//     }
// 	else {
// 	return redirect('/admin');
// 	}
// 	}
//
// 	public function delete($id)
// 	{
// 	if(Auth::check() && (Auth::user()->admin == '1'))
// 	{
// 	$aboutme = Aboutme::find($id);
// 	$aboutme->delete();
// 	return redirect('/admin/aboutme')->with('success', 'aboutme has been successfully Deleted');
// 		}
// 	else {
// 	return redirect('/admin');
// 	}
// }
}
