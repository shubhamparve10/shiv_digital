<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;
use Mail;

class FeedbackController extends Controller
{
	public function index()
    {
			$feedbacks = Feedback::paginate(10);
			return view('public.testimonial', ['results' => $feedbacks]);

			
    }

		public function create()
			{
					return view('public.feedbackform');
					
			}

			public function insert(Request $request)
			{
					$feedback = new Feedback();
					$feedback->name = $request->input('name');
					$feedback->cnumber = $request->input('cnumber');
					$feedback->email = $request->input('email');
					$feedback->exp = $request->input('star');
					$feedback->caption = $request->input('caption');

					$name = $request->get('name');
					$email = $request->get('email');
					$cnumber = $request->get('cnumber');
					$caption = $request->get('caption');
					$exp = $request->get('star');
					$data = array('name'=>$name,'email'=>$email,'cnumber'=>$cnumber,'caption'=>$caption,'exp'=>$exp);
					// 	Mail::send('mail0', $data, function($message) {
					// 		 $message->to('quickinteriordotin@gmail.com', 'Quick Interior')->subject
					// 				('Customer Feedback Notifications');
					// 		 $message->from('quickinteriordotin@gmail.com','Quick Interior');
					// 	});
					// 	Mail::send('mail3', $data, function($message) use ($feedback) {
					// 		$message->to($feedback->email,$feedback->name)->subject
					// 		('Thanking you from Quick Interior');
					// 		 $message->from('quickinteriordotin@gmail.com', 'Quick Interior');
					// 		   });

					$feedback->save();
					return redirect('/')->with('success', 'feedback has been successfully created');
		}


}
