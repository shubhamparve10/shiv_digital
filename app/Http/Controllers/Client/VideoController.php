<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Tag;
use App\Video;

class VideoController extends Controller
{
    public function index()
    {
        return view('public.videos');
    }

    public function getVideos(Request $request, $searchval = 'all')
    {
        if($searchval === 'all')
        {
            $videos = Video::paginate(30);
            return $videos;
        }
        else
        {
            $tag = Tag::where('tag', $searchval)->first();
            $videos = $tag->videos()->paginate(30);
            return $videos;
        }
    }
}
