<?php
namespace App\Http\Controllers\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Tag;
use App\Userimage;
use Auth;

class UserimagesController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */


	public function index()
    {
			return view('public.userimages');
    }

    public function getImages(Request $request, $searchval = 'all')
    {

        if($searchval === 'all')
        {
            $images = UserImage::paginate(30);
            return $images;
        }
        else
        {
            $tag = Tag::where('tag', $searchval)->first();
            $images = $tag->userimages()->paginate(30);
            return $images;
        }
		}
}
