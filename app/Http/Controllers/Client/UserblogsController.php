<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;

use App\Userblogs;

use App\Tag;

use Auth;
use Mail;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserblogsController extends Controller
{

    public function index()
    {
        return view('blogs.blogs');
    }


    public function getblogs(Request $request, $searchval = 'all')
    {
        if($searchval === 'all')
        {
            $blogs = Userblogs::paginate(10);
            return view('public.blogsearch', ['results' => $blogs]);
        }
        else
        {
          $tag = Tag::where('tag', $searchval)->first();
          $blogs = $tag->blogs()->paginate(30);
          return $blogs;
        }
    }

    public function browse(Request $request)
    {
        $results = Userblogs::orderBy('created_at', 'desc')->paginate(10);
        return view('public.blogsearch', ['results' => $results]);
    }

    public function home_page_browse(Request $request)
    {
        $results = Userblogs::orderBy('created_at', 'desc')->paginate(10);
        return view('public.homepage', ['results' => $results]);
    }

    public function search(Request $request)
    {
        $results = $c = collect();
        if($request->input('searchNavbar') !== null)
        {
            $tag = Tag::where('tag', $request->input('searchNavbar'))->first();
            if(!$tag == null)
            {
                $results = $tag->blogs()->orderBy('created_at', 'desc')->paginate(10);
                session()->put('blogSearchTag', $request->input('searchNavbar'));
            }
            return view('public.blogsearch', ['results' => $results]);
        }
        else
        {
            if(session()->has('blogSearchTag'))
            {
                $val = session('blogSearchTag');
                $results = Tag::where('tag', $val)->first()->blogs()->orderBy('created_at', 'desc')->paginate(10);
                return view('public.blogsearch', ['results' => $results]);
            }
        }
    }

    public function showblog(Request $request, $id)
    {
        $blog = Userblogs::find($id);
        return view('public.showblog', ['blog' => $blog]);
    }

    public function order(Request $request)
    {
            $contact = new Contact();
            $contact->name = $request->get('name');
            $contact->email = $request->get('email');
           $contact->phone = $request->get('phone');
           $contact->catg = $request->get('catg');
           $contact->shipad = $request->get('shipad');

           $contact->message = $request->get('message');

           $name = $request->get('name');
            $email = $request->get('email');
           $phone = $request->get('phone');
           $catg = $request->get('catg');
           $shipad = $request->get('shipad');

           $mymessage = $request->get('message');

           $data = array('name'=>$name, 'email'=>$email,'phone'=>$phone,'catg'=>$catg,'shipad'=>$shipad,'mymessage'=>$mymessage );

           $data1 = array('name'=>$name, 'email'=>$email,'phone'=>$phone,'catg'=>$catg,'shipad'=>$shipad,'mymessage'=>$mymessage );
           
            Mail::send('mail5', $data, function($message)  {
                $message->to('shivanshcreation04@gmail.com', 'Quick Interior')->subject
                ('Customer Notifications');
                $message->from('shivanshcreation04@gmail.com', 'Quick Interior');
            });
           Mail::send('mail5', $data, function($message) use ($contact) {
            $message->to($contact->email,$contact->name)->subject
            ('Thanking you from Quick Interior');
             $message->from('shivanshcreation04@gmail.com', 'Quick Interior');
               });


   
       return redirect('/')->with('success', 'contact us has been successfully created');
       }
}
