<?php

namespace App\Http\Controllers\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Image;

class ImageController extends Controller
{
	public function index()
    {
        $images = Image::all();
        //return $images;
        return view('public.images',['images'=>$images]);
    }


    public function getImages(Request $request, $searchval = 'all')
    {
        if($searchval === 'all')
        {
            $images = Image::paginate(30);
            return $images;
        }
        else
        {
            $tag = Tag::where('tag', $searchval)->first();
            $images = $tag->images()->paginate(30);
            return $images;
        }
    }
}
