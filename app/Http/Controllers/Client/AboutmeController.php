<?php
namespace App\Http\Controllers\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Aboutme;
use App\Tag;
use Auth;
class AboutmeController extends Controller
{
    public function index()
    {
        return view('public.aboutme');
    }

    public function getAboutme(Request $request, $searchval = 'first')
    {
        if($searchval === 'first')
        {
            $aboutme = Aboutme::paginate(10);
            return view('public.aboutme', ['results' => $aboutme]);
        }
        else
        {
            $tag = Tag::where('tag', $searchval)->first();
            $aboutme = null;
            if($tag)
            {
                $articles = $tag->aboutme()->paginate(10);
            }
            return view('public.aboutme', ['results' => $aboutme]);
        }
    }

    public function browse(Request $request)
    {
        $results = Aboutme::orderBy('created_at', 'desc')->paginate(10);
        return view('public.aboutme', ['results' => $results]);
    }

    // public function search(Request $request)
    // {
    //     $results = $c = collect();
    //     if($request->input('searchNavbar') !== null)
    //     {
    //         $tag = Tag::where('tag', $request->input('searchNavbar'))->first();
    //         if(!$tag == null)
    //         {
    //             $results = $tag->userimages()->orderBy('created_at', 'desc')->paginate(10);
    //             session()->put('articleSearchTag', $request->input('searchNavbar'));
    //         }
    //         return view('public.articlesearch', ['results' => $results]);
    //     }
    //     else
    //     {
    //         if(session()->has('articleSearchTag'))
    //         {
    //             $val = session('articleSearchTag');
    //             $results = Tag::where('tag', $val)->first()->articles()->orderBy('created_at', 'desc')->paginate(10);
    //             return view('public.articlesearch', ['results' => $results]);
    //         }
    //     }
    // }

    // public function showArticle(Request $request, $id)
    // {
    //     $article = UserImage::find($id);
    //     return view('public.showarticle', ['article' => $article]);
    // }


}
