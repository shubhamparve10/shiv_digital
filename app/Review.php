<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Review extends Model
{
	public function user()
    {
        return $this->belongsTo('App\User');
    }

		public $fillable = [
 			 'name',
  		 'cnumber',
			 'bdate',
			 'adate',
			 'type',
			 'dest',
 			 'caption',
			 'caption1',
			 'rating',
	 	 ];

 	 public $timestamps = false;
}
