<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	public $timestamps = false;

    public function images()
    {
        return $this->belongsToMany('App\Image');
    }

	public function videos()
	{
		return $this->belongsToMany('App\Video');
	}

	public function userimages()
    {
        return $this->belongsToMany('App\Userimage');
    }

    public function blogs()
	    {
	        return $this->belongsToMany('App\Userblog');
	    }
}
