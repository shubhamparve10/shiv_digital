<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


	class Uniform extends Model
{


	 public $fillable = [
			 'name',
			 'email',
             'phone',
             'school',
             'message',
	 ];

	 public $timestamps = false;

}
