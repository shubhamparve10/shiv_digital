<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function update(User $user)
	{
		return $user->admin === 1;
	}

	public function create(User $user)
	{
		return $user->admin === 1;
	}

	public function insert(User $user)
	{
		return $user->admin === 1;
	}

	public function edit(User $user)
	{
		return $user->admin === 1;
	}

	public function delete(User $user)
	{
		return $user->admin === 1;
	}
}
