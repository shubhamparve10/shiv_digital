var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    paginationClickable: true,
    spaceBetween: 30,
    autoplay: 2500,
    loop: true,
    autoplayDisableOnInteraction: false
});

$(".modal-fullscreen").on('show.bs.modal', function() {
    setTimeout(function() {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    }, 0);
});
$(".modal-fullscreen").on('hidden.bs.modal', function() {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
});

//Uncomment for modal functionality
$("#myModal").modal("show");

$(document).ready(function() {
    window.setTimeout(function() {
        $("#myModal").modal("hide");
    } , 5000);
});
