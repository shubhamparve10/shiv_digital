function chatBot() {
	
	// current user input	
	this.input;
	var count=0;

	

	/**
	 * respondTo
	 * 
	 * return nothing to skip response
	 * return string for one responseb
	 * return array of strings for multiple responses
	 * 
	 * @param input - input chat string
	 * @return reply of chat-bot
	 */
	this.respondTo = function(input) {
		//
		this.input = input.toLowerCase();
		// 
		if(this.match('(hi|hello|hey|hola|howdy)(\\s|!|\\.|$)') && count==0)
		{	
			count++;
			document.getElementById("a1").value=this.input;
			return "For Free Quote Please share your 'First Name' & 'Last Name' with us.. ( For e.g., 'Tanmay Wagh' )";
		} 
		if(this.match(/^[a-z]+\s[a-z]+$/) && count==1)
		{
			count++;
			document.getElementById("a2").value=this.input; 
			return "& Also share your '10-digit Mobile No.' ( For e.g. '7894561230' )";
		}
		if(this.match(/^[a-z]+$/) && count==2)
		{
		 return "Please Enter '10-digit Moobile No.' ( For e.g. 7894561230 )";
		}
		if(this.match(/^[7|8|9]\d{9}$/) && count==2)
		{
			count++;
			document.getElementById("a3").value=this.input; 
			return ["Which 'Service' do you want?  1.Interior Designing 2.Space Management  3.Interior Decorating 4.Civil Work 5.Tilling Work 6.Renovation work 7.Painting 8.Waterproofing 9.Furniture 10.False ceiling","Enter your 'Choice': ( e.g '1' or 'interior designing' )"];
		}
		//a4
		if(this.match('(1|interior designing|designing)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Interior Designing';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(2|space management|space)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Space Management';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(3|interior decorating|decorating)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Interior Decorating';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(4|civil work|civil)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Civil Work';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(5|tilling work|tilling)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Tilling Work';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(6|renovation work|renovation)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Renovation Work';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(7|painting|paint)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Painting';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(8|water proofing|waterproofing|water)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Water Proofing';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(9|furniture)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='Furniture';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		if(this.match('(10|false ceiling|false|ceiling)') && count==3)
		{	
			count++;
			document.getElementById("a4").value='False Ceiling';
			return "Which 'Day'  will be preferable for the Telephonic Conversation? ( for e.g. 'Monday' )";
		}
		//a5
		if(this.match('(monday|tuesday|wednesday|thursday|friday|saturday|sunday)(\\s|!|\\.|$)') && count==4)
		{
			count++;
			document.getElementById("a5").value=this.input;
			return "And what 'Time[HH:MM] AM/PM' will be preferable for the Telephonic Conversation? (For e.g. '11.00 AM')";
		}

		if(this.match(/^[0-2][0-9][.|:][0-5][0-9]\s[ap][m]$/) && count==5)
		{
			count++;
			document.getElementById("a6").value=this.input;
			var name=document.getElementById("a2").value;
			document.getElementById('hide1').style.display = "none";
			document.getElementById('show1').style.display = "block";
			//document.getElementById('close1').style.display = "block";
			return [" Thank You "+name.charAt(0).toUpperCase()+name.substr(1)+"","Nice to talk with you. We will contact you soon. 'Please Press the Submit Button.'"];
		}
		if(this.match('^no+(\\s|!|\\.|$)'))
		{
			return "Don't be such a negative nancy :(";
		}
		if(this.match('(cya|bye|see ya|ttyl|talk to you later)'))
		{
			return ["Alright, see you around", "Good teamwork!"];
		}
		if(this.match('(dumb|stupid|is that all)'))
		{
			return ["Hey i'm just a proof of concept", "You can make me smarter if you'd like"];
		}
		if(this.match('(exit|Exit)'))
		{
			//return window.close();
		}
		if(this.input == 'noop')
		{	return;}
		
		
		return [input+" what?","Sorry but I dont understand what you are saying..Please check the previous message and reply correctly.."];
	}

	
	
	/**
	 * match
	 * 
	 * @param regex - regex string to match
	 * @return boolean - whether or not the input string matches the regex
	 */
	this.match = function(regex) {
	
		return new RegExp(regex).test(this.input);
	}
}
