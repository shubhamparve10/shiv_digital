<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateuserblogsTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tag_userblogs'))
        {
            Schema::create('tag_userblogs', function(Blueprint $table){
                $table->integer('userblogs_id')->unsigned();
                $table->integer('tag_id')->unsigned();

                $table->foreign('userblogs_id')->references('id')->on('userblogs');
                $table->foreign('tag_id')->references('id')->on('tags');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(Schema::hasTable('tag_userblogs'))
        {
        	Schema::drop('tag_userblogs');
        }
    }
}
