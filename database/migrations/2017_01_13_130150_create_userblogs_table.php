<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateuserblogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('userblogs', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('path', 255);
            $table->integer('rating');
            $table->string('title', 255);
            $table->text('caption');
            $table->text('shortdescription',100);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userblogs');
    }
}
