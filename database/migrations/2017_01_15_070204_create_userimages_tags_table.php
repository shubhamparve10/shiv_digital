<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserimagesTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tag_userimage'))
        {
            Schema::create('tag_userimage', function(Blueprint $table){
                $table->integer('userimage_id')->unsigned();
                $table->integer('tag_id')->unsigned();

                $table->foreign('userimage_id')->references('id')->on('userimages');
                $table->foreign('tag_id')->references('id')->on('tags');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(Schema::hasTable('tag_userimage'))
        {
        	Schema::drop('tag_userimage');
        }
    }
}
