<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
	/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('reviews', function (Blueprint $table) {
			      $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('path', 255);
            $table->text('name', 15);
            $table->string('cnumber',10);
            $table->float('rating');
            $table->text('type');
            $table->text('dest');
            $table->date('adate');
            $table->text('bdate');
            $table->text('caption');
            $table->text('caption1');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
