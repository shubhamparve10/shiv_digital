<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('videos'))
        {
            Schema::create('videos', function(Blueprint $table){
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('path', 255);
                $table->integer('rating');
                $table->string('title', 255);
                $table->text('caption');
                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users');
            });
        }

		if(!Schema::hasTable('tag_video'))
        {
            Schema::create('tag_video', function(Blueprint $table){
                $table->integer('video_id')->unsigned();
                $table->integer('tag_id')->unsigned();

                $table->foreign('video_id')->references('id')->on('videos');
                $table->foreign('tag_id')->references('id')->on('tags');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(Schema::hasTable('tag_video'))
        {
            Schema::drop('tag_video');
        }

		if(Schema::hasTable('videos'))
        {
            Schema::drop('videos');
        }
    }
}
