<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Shiv Digital</title>
    <link rel="shortcut icon" href="{{ url('images/345.png') }}" />
    
 
	  <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/readable/bootstrap.min.css"
		rel="stylesheet" integrity="sha384-Li5uVfY2bSkD3WQyiHX8tJd0aMF91rMrQP5aAewFkHkVSTT2TmD2PehZeMmm7aiL" crossorigin="anonymous">
     <link href="https://fonts.googleapis.com/css?family=Noto+Sans&display=swap" rel="stylesheet">
	<style>

  

  #feedback {
			float: right;
			position: fixed;
			top: calc(56% - 47px);
			left: 0;
      
		}

	}

	@media (max-width: 799px) {
		#feedback {
			float: right;
			position: fixed;
			top: calc(90% - 50px);
			left: 0;
		}
	}
		#feedback a {
			background: #FF4500;
			border-radius: 5px 0 0 5px;
			box-shadow: 0 0 3px rgba(0, 0, 0, .3);
			border: 3px solid #fff;
			border-left: 0;
			display: block;
			padding: 20px 12px;
			transition: all .2s ease-in-out;
      
      
		}

		#feedback a:hover {
			padding-left: 20px;
		}


  
    
  .dropdown-menu>li>a:hover{
      background-color:gray;
    }
    .navbar-inverse .navbar-nav>li>a {
      color: black;
      font-family: georgia;

    }

    .navbar-inverse .navbar-nav>li>a:hover {
      color:#e27d09;
      background-color:	#DAA520;
    }
    html {
      position: relative;
      min-height: 100%;
    }
  

body{
      background-color:#FFFACD;
     
    }
    .navbar{
      box-shadow: 0 2px px -1px rgba(0,0,0,3);
    }
    .dropdown-submenu {
    position: relative;
}
.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}
.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}
/* submenu arrow hide */
/* .dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
} */
.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}
.dropdown-submenu.pull-left {
    float: none;
}
.dropdown-submenu.pull-left>.dropdown-menu {
    /* left: -100%; */
    /* margin-left: 10px; */
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
.pop{
	position: fixed;
	bottom: 30px;
	right: 40px;
}
#font{
  font-family:Arial, Helvetica, sans-serif;
}
a:hover{
  text-decoration:none  ;
}

@media (min-width: 800px) {
#feedback12 {
			float: right;
			position: fixed;
			top: calc(50% - 47px);
			left: 0;
		}

	}

	@media (max-width: 799px) {
		#feedback12 {
			float: right;
			position: fixed;
			top: calc(90% - 50px);
			left: 0;
		}
	}
		#feedback12 a {
			background: #FF4500;
			border-radius: 5px 0 0 5px;
			box-shadow: 0 0 3px rgba(0, 0, 0, .3);
			border: 3px solid #fff;
			border-left: 0;
			display: block;
			padding: 20px 12px;
			transition: all .2s ease-in-out;
		}

		#feedback12 a:hover {
			padding-left: 20px;
		}

a{
  padding:2px;
  text-decoration:none;
}
.glyphicon-earphone {
  animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both;
  transform: translate3d(0, 0, 0);

}
@keyframes shake {
  10%, 90% {
    transform: translate3d(-1px, 0, 0);
  }
  
  20%, 80% {
    transform: translate3d(2px, 0, 0);
  }

  30%, 50%, 70% {
    transform: translate3d(-4px, 0, 0);
  }

  40%, 60% {
    transform: translate3d(4px, 0, 0);
  }
}
#text{
  font-family: 'Noto Sans', sans-serif;
}
div.navbar-header{
  height:30px;
  min-height:75px;
}
.navbar-nav>li{
  margin-top:5px;
  margin-left:2px;
  padding:0px;

  text-transform: uppercase;
  }
div.container-fluid{
  min-height:0px;
}
ul>li>a{
  font-size:18px;
  font-family:  sans-serif;
 
  

}
.col-md-4{
  text-align:center;
}
 




nav.navbar ul li a {
  position:relative;
  
  z-index: 1;
}
nav.navbar ul li a:hover {
  color: #91640F;
  
}
nav.navbar ul li a:after {
  display: block;
  position: absolute;
  border-radius:90px;
  top: 0;
  left: 0;
  /* bottom: 0; */
  right: 0;
  margin: auto;
  width: 100%;
  height: 1px;
  content: '.';
  color: transparent;

  /* color: transparent; */
  /* background: #bbf50c; */
  background: #0557E8;
  
  visibility: none;
  opacity: 0;
  z-index: -1;
  /* height:10px; */
  padding:15px 0;
  margin-bottom:1px;
  
    
}
nav.navbar ul li a:hover:after {
  opacity: 1;
  visibility: visible;
  height: 100%;
  /* height:40px; */
}

.widgetPosition-Bottom-Right {
    position: fixed;
    bottom: 50px;
    right: 2px;
    margin-top:30px;
}
.whatsappWidget {
    height: 3.5em;
    width: 3.5em;
    border-radius: 50%;
    z-index: 16;
    color: #fff;
    text-align: center;
    display: table;
    z-index: 100;
    transition-property: top;
    transition-duration: .3s;
    transition-timing-function: ease-in-out;
}
@-webkit-keyframes blinker {
  from {opacity: 1.0;}
  to {opacity: 0.0;}
}

.blink{
	text-decoration: blink;
	-webkit-animation-name: blinker;
	-webkit-animation-duration: 0.6s;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-timing-function:ease-in-out;
	-webkit-animation-direction: alternate;
}

.site-footer
{
  /* background-color:#26272b; */
  background-color:#C568E5;
 
  padding:5px 0 5px;
  font-size:14px;
  line-height:24px;
  color:black;
  /* font-family: Georgia,"Times New Roman",Times,serif; */
}
.site-footer hr
{
  border-top-color:#bbb;
  opacity:0.5
}
.site-footer hr.small
{
  margin:20px 0
}
.site-footer h6
{
  color:#fff;
  font-size:16px;
  text-transform:uppercase;
  margin-top:5px;
  letter-spacing:2px
}
.site-footer a
{
  color:blue;
}
.site-footer a:hover
{
  color:#FF0000;
  text-decoration:none;
}
.footer-links
{
  padding-left:0;
  list-style:none
}
.footer-links li
{
  display:block;
  
}
.footer-links a
{
  color:black;

}
.footer-links a:active,.footer-links a:focus,.footer-links a:hover
{
  color:blue;
  text-decoration:none;
}
.footer-links.inline li
{
  display:inline-block
}
.site-footer .social-icons
{
  text-align:right
}
.site-footer .social-icons a
{
  width:40px;
  height:40px;
  line-height:40px;
  margin-left:6px;
  margin-right:0;
  border-radius:100%;
  background-color:#33353d
  
}
.copyright-text
{
  margin:0
}
@media (max-width:991px)
{
  .site-footer [class^=col-]
  {
    margin-bottom:30px
  }
}
@media (max-width:767px)
{
  .site-footer
  {
    padding-bottom:0
  }
  .site-footer .copyright-text,.site-footer .social-icons
  {
    text-align:center
  }
}
.social-icons
{
  padding-left:0;
  margin-bottom:0;
  list-style:none
}
.social-icons li
{
  display:inline-block;
  margin-bottom:4px
}
.social-icons li.title
{
  margin-right:15px;
  text-transform:uppercase;
  color:#96a2b2;
  font-weight:700;
  font-size:13px
}
.social-icons a{
  background-color:#eceeef;
  color:#818a91;
  font-size:16px;
  display:inline-block;
  line-height:44px;
  width:44px;
  height:44px;
  text-align:center;
  margin-right:8px;
  border-radius:100%;
  -webkit-transition:all .2s linear;
  -o-transition:all .2s linear;
  transition:all .2s linear
}
.social-icons a:active,.social-icons a:focus,.social-icons a:hover
{
  color:#fff;
  background-color:#29aafe
}
.social-icons.size-sm a
{
  line-height:34px;
  height:34px;
  width:34px;
  font-size:14px
}
.social-icons a.facebook:hover
{
  background-color:#3b5998
}
.social-icons a.twitter:hover
{
  background-color:#00aced
}
.social-icons a.linkedin:hover
{
  background-color:#007bb6
}
.social-icons a.dribbble:hover
{
  background-color:#ea4c89
}
@media (max-width:767px)
{
  .social-icons li.title
  {
    display:block;
    margin-right:0;
    font-weight:600
  }
}

/* footer */
a {
    color: #e4e9f3; */
      text-decoration: none;
}

.navbar .navbar-nav>li>a {
    padding: 8px 12px;
    border-radius: 90px;
    /* border-color: black; */

}


nav.navbar ul li a {
  position:relative;
  
  z-index: 1;
}
nav.navbar ul li a:hover {
  color: #91640F;
}
nav.navbar ul li a:after {
  display: block;
  position: absolute;
  border-radius: 50px;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  margin: auto;
  width: 100%;
  height: 1px;
  content: '.';
  color: transparent;
  background: #4EE2EC;
  visibility: none;
  opacity: 0;
  z-index: -1;
    
}
nav.navbar ul li a:hover:after {
  opacity: 1;
  visibility: visible;
  height: 100%;
}
nav ul li a, nav ul li a:after, nav ul li a:before {
    transition: all .9s;
}

.widgetPosition-Bottom-Right {
    position: fixed;
    bottom: 50px;
    right: 2px;
    margin-top:10px;
} 
.whatsappWidget {
    height: 3.5em;
    width: 3.5em;
    border-radius: 50%;
    z-index: 16;
    color: #fff;
    text-align: center;
    display: table;
    z-index: 100;
    transition-property: top;
    transition-duration: .9s;
    transition-timing-function: ease-in-out;
}
</style>

@yield('css')
<!-- Scripts -->
<script>
  window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
    ]); ?>
</script>

</head>
<body style="margin-top: 78px; margin-bottom: 6px; ">
    <div id="app">
    <nav class="navbar navbar-inverse bg-inverse navbar-fixed-top " style="background-color:#A74AC7	; box-shadow: 0 0px 6px 0 rgba(0,0,0,0.2); ">
            <div class="navbar-top hidden-xs" >
                
            </div>
        
            <div class="container-fluid" >
            <!-- &nbsp; -->
            <div class="header">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                   
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"data-target="#app-navbar-collapse"  style="border-color:black">
                        <span class="sr-only"  >Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->


                   <!-- <a class="navbar-brand"  href="{{ url('/') }}" ><img src="{{ url('images/ .jpg') }}" style="border-radius:50%;padding:0px; margin-left:70px;margin-top:0px;" alt="logo" height="75" width="90">  -->
                   <!-- <h><a href="{{ url('/') }}"> <img  src="{{asset('images/345.png')}}"  style="  margin-top:-30px;margin-left:0px;height:100px;width:260px;"> </a></h2> -->
                
    <h3><img src="{{asset('images/logo.png')}}"  style="width:40px;height:40px; margin-left:25px; " > 
<a href="{{ url('/') }}" class="scrollto" style="margin-top:40px;  color:White;" ><b>SHIVPICS&nbsp;&nbsp;&nbsp; </b></a></h3>
    <!-- Uncomment below if you prefer to use an image logo -->
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse" >
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav" style="float:right;margin-right:40px; " >    

                     <!-- <li><a href="{{ url('/videos/gallery') }}"font-size:16px;" >Products</a></li>  -->
                       <li ><a style="color:white; font-family: Sora, sans-serif;" href="{{ url('/') }}" onMouseOver="this.style.color='black'" onMouseOut="this.style.color='white'"  ><b>Home</b></a></li>
                       <li><a  style="color:white; font-family: sans-serif "href=" {{ url('/userblogs') }}" onMouseOver="this.style.color='black'" onMouseOut="this.style.color='white'" ><b>Products</b>  </a></li>
                     <li><a style="color:white; font-family: sans-serif" href="{{ url('/images/gallery/') }}"onMouseOver="this.style.color='black'" onMouseOut="this.style.color='white'" ><b>Images</b> </a></li>  
                        <!--  <li><a style="color:white;font-family: sans-serif" href="{{ url('/videos/gallery/') }}"onMouseOver="this.style.color='white'" onMouseOut="this.style.color='white'" ><b>Videos </b></a></li>  
                       <li><a style="color:white;font-family: sans-serif" href="{{ url('/uniform')  }}"onMouseOver="this.style.color='white'" onMouseOut="this.style.color='white'" ><b>Uniform </b> </a></li>
                       <li><a style="color:white;font-family: sans-serif" href="{{ url('/dealership') }}"onMouseOver="this.style.color='white'" onMouseOut="this.style.color='white'" ><b>Take-Dealership</b> </a></li> -->
                       <li><a style="color:white;font-family: sans-serif" href="{{ url('/about') }}"onMouseOver="this.style.color='black'" onMouseOut="this.style.color='white'" ><b>About Us</b></a></li>
                       <li><a style="color:white;font-family: sans-serif" href="{{ url('/contact') }}"onMouseOver="this.style.color='black'" onMouseOut="this.style.color='white'" ><b>Contact Us</b></a></li>
                       <li><a style="color:white;font-family: sans-serif" href="{{ url('/testimonial') }}"onMouseOver="this.style.color='black'" onMouseOut="this.style.color='white'" ><b>Feedback </b></a></li>

                 </ul> <br> 
                 @if (Auth::guest())
                            <!--<li ><a href="{{ url('/login') }}">Login</a></li>-->
                            <!--li><a href="{{ url('/register') }}">Register</a></li-->
                        @else
                            <li class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  <b style="color:white">  {{ Auth::user()->name }} </b><span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a
                                         href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif                                                          

   <!-- Right Side Of Navbar -->
      <div>     
    </div>
  </div>
</nav>
        @yield('content')
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	@yield('js')


  <footer class="site-footer">
      <div class="container" style="width: 90%">
        <div class="row">

          <div class="col-xs-10 col-md-4">
        <h3>  <a href="{{ url('/') }}" class="scrollto" style="margin-top:40px;  color:White;" ><b>SHIVPICS&nbsp;&nbsp;&nbsp; </b></a></h3>

          <!-- <h2><a href="{{ url('/') }}"> <img  src="{{asset('images/345.png')}}"  style="  margin-top:-30px;margin-left:0px;height:100px;width:260px;"> </a></h2> -->
          </div>
         

          <div class="col-xs-9 col-md-5">
            <h6 style="color:black">CONTACT</h6>
            <ul class="footer-links" style="font-family: sans-serif">
              <li style="margin-right:29px"><i class="glyphicon glyphicon-phone blink " style="color:blue;font-size:18px;"></i><span style="color:white;font-size:20px;">&nbsp <a href="Tel: +91-8983691455">+91-8983691455</a></span> </li>
             <li><i class="glyphicon glyphicon-envelope blink  " style="color:#D44638;font-size:18px;"></i><span style="color:black;font-size:20px;">  <a href="mailto:shivanshcreation04@gmail.com"> shivanshcreation04@gmail.com</a></span></li> 
             </ul>
          </div>
              
          <div class="col-xs-12 col-md-3">
             <ul class="footer-links">
             <a href="https://www.facebook.com/shubham.parve.50/"><i class="fa fa-facebook-official"  style="font-size:28px;margin-left:10px;color:#3b5998 "></i></a>&nbsp;
             <a href= "#" style="color:#cd486b; font-size:28px;" class="fa fa-instagram" ></a>&nbsp;&nbsp;&nbsp;
             <a href="mailto:?subject=&body=:%20" style="color:#D44638; font-size:28px;" class="fa fa-envelope"  ></a></li><br>
             <br>
              <li> <a href="{{ url('/') }}"  >Home</a> | 
             <a href="{{ url('/contact') }}">Contact </a></li>
             </ul>
          </div>
           
          </div>
          </div>
               
          <div class="container">
        <div class="row">
        <u>  <div class="col-md-12 col-sm-6 col-xs-12"style="margin-bottom:20px; line-height: 10px;">
           <center> <p class="copyright-text">  &copy; 2020 All Rights Reserved by 
         <a href=" ">IT Solution</a>.</u>
            </p><center>
          </div>
 
        </div>
      </div>
              
            
   
       
      
</footer>




<!-- WhatsApp (Icon) number -->

<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+91 8793573702", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
</body>

</html>
<div class="container">
			<div id="feedback">
			<a href="{{ url('/feedbacks') }}" type="button" class="btn btn-info btn-lg" style="height:20%;font-size:20px">
				<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSI5LjI5OXB4IiBoZWlnaHQ9IjUwLjYyNXB4IiB2aWV3Qm94PSIwIDAgOS4yOTkgNTAuNjI1IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA5LjI5OSA1MC42MjUiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwYXRoIGZpbGw9IiNGRkZGRkYiIGQ9Ik0zLjUxNiw0Ni44N3YxLjYzNWg1LjY2N3YwLjk3NEgzLjUxNnYxLjE0N2gtMC40NGwtMC4zNTItMS4xNDdIMi4zNjdDMC43ODksNDkuNDc4LDAsNDguNzg4LDAsNDcuNDA5YzAtMC4zNCwwLjA2OC0wLjczOCwwLjIwNS0xLjE5NWwwLjc3OSwwLjI1MmMtMC4xMjEsMC4zNzUtMC4xODIsMC42OTUtMC4xODIsMC45NjFjMCwwLjM2NywwLjEyMiwwLjY0LDAuMzY2LDAuODE0YzAuMjQ0LDAuMTc2LDAuNjM2LDAuMjY0LDEuMTc1LDAuMjY0SDIuNzZWNDYuODdIMy41MTZ6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTkuMjk5LDQyLjk4NWMwLDAuOTQ5LTAuMjg5LDEuNjk3LTAuODY2LDIuMjQ2Yy0wLjU3OCwwLjU0OS0xLjM4MiwwLjgyNC0yLjQwOCwwLjgyNGMtMS4wMzYsMC0xLjg1Ny0wLjI1Ni0yLjQ2Ny0wLjc2NmMtMC42MS0wLjUxMS0wLjkxNC0xLjE5My0wLjkxNC0yLjA1NGMwLTAuODA1LDAuMjY1LTEuNDQsMC43OTQtMS45MDljMC41MjktMC40NywxLjIyOC0wLjcwMywyLjA5NS0wLjcwM2gwLjYxNXY0LjQyNGMwLjc1NC0wLjAyMSwxLjMyNi0wLjIxMSwxLjcxNy0wLjU3MlM4LjQ1LDQzLjYwNiw4LjQ1LDQyLjk1YzAtMC42OTEtMC4xNDUtMS4zNzUtMC40MzQtMi4wNTFoMC44NjZjMC4xNDgsMC4zNDQsMC4yNTUsMC42NjgsMC4zMTksMC45NzVDOS4yNjcsNDIuMTgxLDkuMjk5LDQyLjU1MSw5LjI5OSw0Mi45ODV6IE0zLjQ1Nyw0My4yNDljMCwwLjUxNywwLjE2OCwwLjkyNiwwLjUwNCwxLjIzMnMwLjgwMSwwLjQ4OCwxLjM5NiwwLjU0M3YtMy4zNTdjLTAuNjEzLDAtMS4wODMsMC4xMzgtMS40MSwwLjQxQzMuNjIxLDQyLjM1LDMuNDU3LDQyLjc0MSwzLjQ1Nyw0My4yNDl6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTkuMjk5LDM2LjI1MmMwLDAuOTQ5LTAuMjg5LDEuNjk3LTAuODY2LDIuMjQ2Yy0wLjU3OCwwLjU0OS0xLjM4MiwwLjgyNC0yLjQwOCwwLjgyNGMtMS4wMzYsMC0xLjg1Ny0wLjI1Ni0yLjQ2Ny0wLjc2NmMtMC42MS0wLjUxLTAuOTE0LTEuMTkzLTAuOTE0LTIuMDUzYzAtMC44MDUsMC4yNjUtMS40NDEsMC43OTQtMS45MXMxLjIyOC0wLjcwMywyLjA5NS0wLjcwM2gwLjYxNXY0LjQyNGMwLjc1NC0wLjAyLDEuMzI2LTAuMjExLDEuNzE3LTAuNTcyczAuNTg2LTAuODY5LDAuNTg2LTEuNTI1YzAtMC42OTEtMC4xNDUtMS4zNzUtMC40MzQtMi4wNTFoMC44NjZjMC4xNDgsMC4zNDQsMC4yNTUsMC42NjgsMC4zMTksMC45NzZTOS4yOTksMzUuODE5LDkuMjk5LDM2LjI1MnogTTMuNDU3LDM2LjUxNmMwLDAuNTE2LDAuMTY4LDAuOTI2LDAuNTA0LDEuMjMyYzAuMzM2LDAuMzA4LDAuODAxLDAuNDg4LDEuMzk2LDAuNTQzdi0zLjM1N2MtMC42MTMsMC0xLjA4MywwLjEzNy0xLjQxLDAuNDFDMy42MjEsMzUuNjE4LDMuNDU3LDM2LjAwOCwzLjQ1NywzNi41MTZ6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTguMzE5LDI3Ljg2MnYwLjA1M2MwLjY1MiwwLjQ0OSwwLjk3OSwxLjEyMSwwLjk3OSwyLjAxN2MwLDAuODQtMC4yODYsMS40OTMtMC44NiwxLjk2UzcuMDQ3LDMyLjU5LDUuOTg5LDMyLjU5Yy0xLjA1OCwwLTEuODgxLTAuMjMzLTIuNDY3LTAuNzAyYy0wLjU4Ni0wLjQ3LTAuODc5LTEuMTIxLTAuODc5LTEuOTU3YzAtMC44NzEsMC4zMTYtMS41MzksMC45NDktMi4wMDVWMjcuODVsLTAuNDYzLDAuMDQxbC0wLjQ1MSwwLjAyM0gwLjA2NHYtMC45NzRoOS4xMTd2MC43OTFMOC4zMTksMjcuODYyeiBNOC40ODMsMjkuODA3YzAtMC42NjQtMC4xOC0xLjE0Ni0wLjU0MS0xLjQ0NHMtMC45NDQtMC40NDgtMS43NS0wLjQ0OEg1Ljk4N2MtMC45MDksMC0xLjU1OSwwLjE1MS0xLjk0NywwLjQ1NWMtMC4zODgsMC4zMDMtMC41ODMsMC43ODUtMC41ODMsMS40NDljMCwwLjU3LDAuMjIyLDEuMDA4LDAuNjY1LDEuMzExUzUuMTksMzEuNTgzLDYsMzEuNTgzYzAuODE5LDAsMS40MzgtMC4xNDksMS44NTYtMC40NTFDOC4yNzQsMzAuODMxLDguNDgzLDMwLjM4OSw4LjQ4MywyOS44MDd6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTIuNjU0LDIxLjg5MWMwLTAuODQ0LDAuMjg4LTEuNDk5LDAuODY0LTEuOTY2YzAuNTc2LTAuNDY3LDEuMzkyLTAuNywyLjQ0Ni0wLjdjMS4wNTUsMCwxLjg3NCwwLjIzNSwyLjQ1OCwwLjcwNmMwLjU4NCwwLjQ3MSwwLjg3NiwxLjEyNCwwLjg3NiwxLjk2YzAsMC40MTgtMC4wNzYsMC44LTAuMjMsMS4xNDZjLTAuMTU0LDAuMzQ2LTAuMzkzLDAuNjM2LTAuNzEzLDAuODd2MC4wN2wwLjgyNiwwLjIwNXYwLjY5N0gwLjA2NHYtMC45NzNoMi4yMTVjMC40OTYsMCwwLjk0MSwwLjAxNiwxLjMzNiwwLjA0N3YtMC4wNDdDMi45NzUsMjMuNDU0LDIuNjU0LDIyLjc4MiwyLjY1NCwyMS44OTF6IE0zLjQ2OSwyMi4wMzJjMCwwLjY2NCwwLjE5LDEuMTQzLDAuNTcxLDEuNDM2czEuMDIyLDAuNDM5LDEuOTI1LDAuNDM5YzAuOTAzLDAsMS41NDgtMC4xNSwxLjkzNy0wLjQ1MWMwLjM5LTAuMzAxLDAuNTg0LTAuNzgzLDAuNTg0LTEuNDQ3YzAtMC41OTgtMC4yMTktMS4wNDMtMC42NTMtMS4zMzZzLTEuMDYyLTAuNDM5LTEuODc4LTAuNDM5Yy0wLjgzNiwwLTEuNDU5LDAuMTQ2LTEuODY5LDAuNDM5UzMuNDY5LDIxLjQxOCwzLjQ2OSwyMi4wMzJ6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTkuMTgzLDEzLjU3N0w4LjI2OSwxMy43N3YwLjA0N2MwLjQwMSwwLjMyLDAuNjc0LDAuNjQsMC44MTYsMC45NThzMC4yMTQsMC43MTYsMC4yMTQsMS4xOTJjMCwwLjYzNy0wLjE2NCwxLjEzNi0wLjQ5MSwxLjQ5N2MtMC4zMjgsMC4zNjEtMC43OTUsMC41NDItMS40LDAuNTQyYy0xLjI5NywwLTEuOTc3LTEuMDM3LTIuMDM5LTMuMTExbC0wLjAzNS0xLjA5SDQuOTM0Yy0wLjUwNCwwLTAuODc2LDAuMTA4LTEuMTE2LDAuMzI1Yy0wLjI0LDAuMjE3LTAuMzYsMC41NjMtMC4zNiwxLjA0YzAsMC41MzUsMC4xNjQsMS4xNDEsMC40OTIsMS44MTZsLTAuNzQ0LDAuMjk5Yy0wLjE3Mi0wLjMxNi0wLjMwNy0wLjY2My0wLjQwNC0xLjA0Yy0wLjA5Ny0wLjM3Ny0wLjE0Ni0wLjc1NS0wLjE0Ni0xLjEzNGMwLTAuNzY2LDAuMTctMS4zMzMsMC41MS0xLjcwMmMwLjM0LTAuMzY5LDAuODg1LTAuNTU0LDEuNjM1LTAuNTU0aDQuMzg0djAuNzIySDkuMTgzeiBNOC40OTYsMTUuNzc0YzAtMC42MDUtMC4xNjYtMS4wODEtMC40OTgtMS40MjdjLTAuMzMyLTAuMzQ2LTAuNzk3LTAuNTE5LTEuMzk2LTAuNTE5aC0wLjU4bDAuMDQxLDAuOTczYzAuMDI3LDAuNzczLDAuMTQ3LDEuMzMxLDAuMzYsMS42NzNjMC4yMTQsMC4zNDIsMC41NDQsMC41MTMsMC45OTMsMC41MTNjMC4zNTIsMCwwLjYxOS0wLjEwNiwwLjgwMy0wLjMxOVM4LjQ5NiwxNi4xNTcsOC40OTYsMTUuNzc0eiIvPjxwYXRoIGZpbGw9IiNGRkZGRkYiIGQ9Ik05LjI5OSw4LjI4NWMwLDAuOTMtMC4yODYsMS42NDktMC44NTgsMi4xNTljLTAuNTcxLDAuNTEtMS4zODEsMC43NjUtMi40MjgsMC43NjVjLTEuMDc1LDAtMS45MDUtMC4yNTktMi40OTEtMC43NzZTMi42NDMsOS4xNzgsMi42NDMsOC4yMjFjMC0wLjMwOSwwLjAzMy0wLjYxNywwLjEtMC45MjZDMi44MSw2Ljk4NiwyLjg4OCw2Ljc0NCwyLjk3Nyw2LjU2OGwwLjgyNiwwLjI5OUMzLjcxNyw3LjA4MiwzLjY0Niw3LjMxNiwzLjU4OSw3LjU3QzMuNTMyLDcuODI0LDMuNTA0LDguMDQ5LDMuNTA0LDguMjQ0YzAsMS4zMDUsMC44MzIsMS45NTcsMi40OTYsMS45NTdjMC43ODksMCwxLjM5Ni0wLjE1OSwxLjgxNS0wLjQ3OGMwLjQyMi0wLjMxOSwwLjYzNC0wLjc5LDAuNjM0LTEuNDE1YzAtMC41MzUtMC4xMTUtMS4wODQtMC4zNDctMS42NDZoMC44NjFDOS4xODgsNy4wOTIsOS4yOTksNy42MzMsOS4yOTksOC4yODV6Ii8+PHBhdGggZmlsbD0iI0ZGRkZGRiIgZD0iTTUuODk2LDQuMTc4QzUuNjU2LDQuMDEsNS4zNDQsMy43NTQsNC45NTcsMy40MUwyLjc2LDEuMzM2VjAuMTgybDIuNzM2LDIuNjAyTDkuMTgzLDB2MS4xNzhMNi4xNDYsMy40NDVsMC42MzMsMC43MzJoMi40MDJ2MC45NjFIMC4wNjR2LTAuOTZoNC44MzRjMC4yMTUsMCwwLjU0NiwwLjAxNiwwLjk5NiwwLjA0N1Y0LjE3OEg1Ljg5NnoiLz48L2c+PC9zdmc+"
					alt="Feedback" title="Feedback Button"  height="60px;" width="18px"/>
			</a>
			</div>
    </div>
    
