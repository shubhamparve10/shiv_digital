@extends('layouts.app')
@section('css')

 
<meta name="viewport" content="width=device-width, initial-scale=1">
 
 
 <style>
    #box {
    margin: 30px auto 0 auto;
    animation: appear 4s  ;
    /* animation: appear 4s infinite; */
  }
  @keyframes appear {
    0% {
      transform: translate3d(-500px, 0px, 0px);
      opacity: 0;
    }
    50%{
      opacity: 0.2;
      transform: translate3d(-350px, 0px, 0px);
    }
    100%{
      opacity: 1;
      transform: translate3d(0px, 0px, 0px);
    }
  }
    display: none;{
    top: 100%;
    left: 0;
    z-index: 99;
  }

body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border:2px solid #111;
  background-color: #333;
  width:69%;
  border-radius:9px;
  color:#ccc;
  
 
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: center;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 22px;
  
  
}
.div2 {
  width:69%;
  height: 40%;  
  padding: 50px;
  border: 1px solid black;
  border-radius:10px;
  
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #bbf50c;
    border-radius:19px;

}

/* Create an active/current tablink class */
. 
/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

/* a:hover {
  background-color: blue;
} */

.btn
{
  font-size: 15px;
  height:40px;
  padding:10px;
  

   
}

input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    color: #468847;
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    color: #B94A48;
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    margin: 2px 0 3px;
    padding: 0;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }

  .parsley-type, .parsley-required, .parsley-equalto, .parsley-pattern, .parsley-length{
   color:#ff0000;
  }
  

 


</style>
@endsection

@section('js')
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
</script>

<script src="http://parsleyjs.org/dist/parsley.js"></script>
<script>
$(document).ready(function(){
    $('#validate_form').parsley();

    
});
</script>
 



@endsection

@section('content')
<body> 
<div class="container" style="margin-top: -1em;">
		<div class="row">
			<div class="col-xs-12 col-xs-offset-0" >
      <center>	<h2 id='box'>
           Dealership
				    </h2></center>
			 			</div>
		</div></div>
 <br>
   
<center><div class="tab"> 
  <button class="tablinks" onclick="openCity(event, 'Step 1')"id="defaultOpen" onMouseOver="this.style.color='#111'" onMouseOut="this.style.color='white'"> Benefits</button>
  <button class="tablinks" onclick="openCity(event, 'Step 2')"id="defaultOpen" onMouseOver="this.style.color='#111'" onMouseOut="this.style.color='white'">Requirements</button>
  <button class="tablinks" onclick="openCity(event, 'Step 3')"id="defaultOpen" onMouseOver="this.style.color='#111'" onMouseOut="this.style.color='white'">Set Meeting</button>
</div>

<div id="Step 1" class="tabcontent  div2" style="background-color:	#FAFFE3; text-align:left; "> 
<center> <h3 style="margin-top:-10px;"><u>Benefits</u></h3></center><br>
  <h4> <li>You can make lot of money</li></h4>
  <h4><li>Professional approach  towords business</li></h4>
  <h4> <li>Quality Fabric & Perfect Fitting </li></h4>
  <h4><li> Continuous & regular supply of T-Shirts, Jeans and Uniforms etc</li></h4>
  <h4> <li>Affordable product range to target large audience</li></h4>
  
    <br><br><center>   <button class="btn btn-primary btn-lg tablinks btn-outline-info " onclick="openCity(event, 'Step 2')" type="button" style="height:50px; width:150px">Next </button></center></div>

<div id="Step 2" class="tabcontent div2" style="background-color:#FAFFE3;text-align:left; ">
<center><h3 style="margin-top:-10px;"><u>Requirements</u></h3></center><br>
     
      <h4><li>Shop at good locality</li></h4>
      <h4><li>Finance of ₹ 10-12 lakh</li></h4>
      <h4><li>Good communication Skill</li></h4>
      <h4><li>Daily 10-12 hrs dedication</li></h4>

       <br><br> <center>  <button class="btn btn-primary btn-lg tablinks btn-outline-info"onclick="openCity(event, 'Step 3')" type="button" style="height:50px; width:150px">Next </button> </center>   
</div>

 <div id="Step 3" class="tabcontent div2" style="background-color:#FAFFE3;text-align:left; ">

<center><h3 style="margin-top:-10px;"><u>Set Meeting</u></h3></center><br>
  
      <form id="validate_form"  method="post" action="{{ url('/setMeeting') }}" name="sentMessage">
						{{ csrf_field() }}
                <div class="control-group form-group">

              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <label style="margin-top:10px; font-size:18px;"><i class="glyphicon glyphicon-pencil"style="margin-right:9px"></i>Full Name:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
                <input type="text" class="form-control" id="name" placeholder="Enter Name" required data-validation-required-message="Please enter your name." name="name" required data-parsley-pattern="[a-zA-Z ]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup">
                <p class="help-block"></p> 
                </div>
              </div>
            </div>

            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                <label style="margin-top:10px;font-size:18px;"><i class="glyphicon glyphicon-phone"style="margin-right:10px"> </i>Phone Number:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
                <input type="tel" class="form-control"     id="phone" required data-validation-required-message="Please enter your phone number." name="phone" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup">
                <p class="help-block"></p>  </div>
                <!-- <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number." name="cnumber" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup"> -->
              </div>
            </div> 
            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                <label style="margin-top:10px;font-size:18px;"> <i class="glyphicon glyphicon-envelope"style="margin-right:10px"> </i>Email Address:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address." name="email" placeholder="Email" required data-parsley-type="email"  data-parsley-trigger="keyup">
                <p class="help-block"></p></div>
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                <label style="margin-top:10px;font-size:18px;"><i class="glyphicon glyphicon-check"style="margin-right:10px"> </i>Message:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <textarea rows="2" cols="20" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="message" placeholder="Message"></textarea><br>
                <p class="help-block"></div></div></div>
            
            <div id="success"></div>
           <center>   <button type="submit" class="btn btn-primary" id="sendMessageButton" style="height:50px;width:150px; ">Set Meeting </button></center> 
          </form> 
      <!-- <button class="btn btn-primary btn-lg tablinks btn-outline-info " onclick="openCity(event, 'Step 1')" type="button">Next </button>     -->
</div></center>


   
</body>
 

	

	
	<br<br><br><br><br><br><br><br> 
@endsection 
