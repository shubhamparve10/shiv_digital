@extends('layouts.app')
@section('js')
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/articles/showarticle.js') }}"></script>
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
	<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('js/articles/showarticle.js') }}"></script>
@endsection
@section('css')
	<link rel="stylesheet" href="{{ asset('css/articles/showarticle.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/justifiedGallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lg-transitions.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/articles/showarticle.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<link rel="shortcut icon" href="images/NeetaTechLogo.jpg" />
	
	<style>
body{background:url(https://www.adsyndicate.in/wp-content/uploads/2018/02/LP-bg-1.jpg);}

#box {
  margin: 30px auto 0 auto;
  animation: appear 4s 2;
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}

</style>
@endsection
    
    
@section('content')
<body>
	<div class="container-fluid banner" style="padding-top:120px;">
		<div class="row" >
			<div class="panel panel-primary panel-heading col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-1" style="border-color:black;">
				<div class="wrapper" style="border-color:black;">
					<div class="heading-title" style="background-color:rgb(0,0,0,0.2); color:black" >
						<center><h2 class="">{{ $article->title }}</h2></center>
					</div>
					
				</div>
<br>
				<div class="panel-body">
				
					<div class="row content-container">
					
					<center><img src="{{ asset($article->path) }}" class="header-image img-responsive"  id='box' style="height:300px; width:300px;" alt="Image Not Found" /></center>
				
						<p class="lead">
						
							{!! $article->caption !!}
						</p>
					</div>
			</div>
			<hr>
			<div class="panel-heading  panel-heading-main" style="background-color:white;color:black;border-color:black;">
				<div class="row">
						<!--<div class="col-md-4" >
							<i class="fa fa-user fa-2x"></i>
							<h4 style="display:inline !important;">{{ $article->user->username }} - {{ $article->created_at->timezone('Asia/Kolkata')->format('d/m/Y')  }}</h4>
						</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >-->
							<h4 style="display:inline !important;color:black;">
								Tags:- <i class="fa fa-tags fa-2x" aria-hidden="true"></i>
								@foreach($article->tags as $tag)
									<a class="small" style="color:black;" href="#"><b>{{ $tag->tag }}, </b></a>
								@endforeach
							</h4>
						</div>


					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="padding-top:1px;padding-bottom:1px;">

						<!--<a href=""whatsapp://send?text={{Request::fullUrl()}}" class="btn btn-raised btn-info" style="margin-top: 0px;font-size:14px;background-color:white;color:green;border-color:black;">
							<i class="fa fa-whatsapp" aria-hidden="true"></i><b>Whatsapp</b>
						</a>--><center>
						<h5>Share on:
						<a class="fa fa-facebook" aria-hidden="true"  href="https://www.facebook.com/sharer.php?u={{Request::fullUrl()}}" class="btn btn-raised btn-info" style="margin-top: 0px; font-size:20px;background-color:white;color:black;border-color:black;">
						</a>&nbsp &nbsp
						<a class="fa fa-twitter"  href="https://twitter.com/share?url={{Request::fullUrl()}}" class="btn btn-raised btn-info" style="margin-top: 0px;font-size:20px;background-color:white;color:black;border-color:black;">
						</a>
						&nbsp &nbsp
						<a class="fa fa-instagram"  href="https://instagram.com/share?url={{Request::fullUrl()}}" class="btn btn-raised btn-info" style="margin-top: 0px;font-size:20px;background-color:white;color:black;border-color:black;">
						</a></h5></Scenter>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	</body>
@endsection
