@extends('layouts.app')

@section('css')
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	
	<style>
	a:hover {
	    background-color: white;
	}
	h2 a:hover {
text-decoration: none;
}
@-webkit-keyframes blinker {
  from {opacity: 1.0;}
  to {opacity: 0.0;}
}
.blink{
	text-decoration: blink;
	-webkit-animation-name: blinker;
	-webkit-animation-duration: 0.6s;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-timing-function:ease-in-out;
	-webkit-animation-direction: alternate;
}

#box {
  margin: 30px auto 0 auto;
  animation: appear 4s 3;
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}
</style>
@endsection

@section('js')

@endsection

@section('content')
	<div class="container" style="margin-top: 10vh;">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
				<div class="page-header">
					<h1 class="text-center" id='box'>Career With Us</h1>
				</div>
<p>We are hiring enthusiastic candidates who are well-trained in technology.</p>
<p>We have successfully completed large range of products. We are working on upcoming technologies. Along with experience our employees also gain knowledge of latest technology. We provide healthy work environment where one can excel his/her skills. So if you are interested and want to work in challenging environment you are most welcome.
</p>
<p>We are seeking for knowledgeable candidates in technologies like:
	<ul class="list-group">
	  <li class="list-group-item"><b>Language: PHP, Javascript</b></li>
		<li class="list-group-item"><b>Framework: Laravel, Wordpress</b></li>
	  <li class="list-group-item"><b>Front End: CSS3, HTML5, Bootstrap</b></li>
		<li class="list-group-item"><b>Database:  SQL, Mongo DB, Maria DB</b></li>
		<li class="list-group-item"><b>Full stack: Mean Stack.</b></li>
	</ul>
</p>
<p>We are offering opportunities for the students willing to work part time. Candidates having keen interest in technologies can apply for job in R&D department. Students or other candidates who are passionate to work are most welcome.</p>
<p>If you are interested send your resume at <a class="blink" href="hr@neetatech.com" style="text-decoration:none;">hr@neetatech.com</a>
</p>
			</div>

		</div>
	</div>

	<h2> </h2>
	<h2> </h2>
	<h2> </h2>


@endsection
