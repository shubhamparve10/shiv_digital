@extends('layouts.app')

@section('css')


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/css/swiper.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
 	
	<link href="css/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/lib/animate/animate.min.css" rel="stylesheet">
  <link href="css/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

 <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/readable/bootstrap.min.css"
		rel="stylesheet" integrity="sha384-Li5uVfY2bSkD3WQyiHX8tJd0aMF91rMrQP5aAewFkHkVSTT2TmD2PehZeMmm7aiL" crossorigin="anonymous">
    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/css/swiper.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
 
	<link rel="stylesheet" href="{{'css/homepage2.css'}}">
   


  <style>

body{
  background-color:#FFFACD;}
#box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
  /* animation: appear 4s infinite; */
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}
  display: none;
  top: 100%;
  left: 0;
  z-index: 99;
}


/* Nav Menu Arrows */

.sf-arrows .sf-with-ul {
  padding-right: 30px;
}

.sf-arrows .sf-with-ul:after {
  content: "\f107";
  position: absolute;
  right: 15px;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
}

.sf-arrows ul .sf-with-ul:after {
  content: "\f105";
}

/* Nav Meu Container */


/*--------------------------------------------------------------
# Sections
--------------------------------------------------------------*/

/* Sections Header
--------------------------------*/
.section-header h3 {
  font-size: 32px;
  color: #111;
  text-transform: uppercase;
  text-align: center;
  font-weight: 700;
  position: relative;
  padding-bottom: 15px;
}

.section-header h3::before {
  content: '';
  position: absolute;
  display: block;
  width: 120px;
  height: 1px;
  background: #ddd;
  bottom: 1px;
  left: calc(50% - 60px);
}


.section-header h3::after {
  content: '';
  position: absolute;
  display: block;
  width: 40px;
  height: 3px;
  background: #1aa3ff;
  bottom: 0;
  left: calc(50% - 20px);
}

.section-header p {
  text-align: center;
  padding-bottom: 30px;
  color: #333;
}

/* Section with background
--------------------------------*/

.section-bg {
  background: #f7f7f7;
}



/* Portfolio Section
--------------------------------*/

#portfolio {
  padding: 60px 0;
}

#portfolio #portfolio-flters {
  padding: 0;
  margin: 5px 0 35px 0;
  list-style: none;
  text-align: center;
}

#portfolio #portfolio-flters li {
  cursor: pointer;
  margin: 15px 15px 15px 0;
  display: inline-block;
  padding: 10px 20px;
  font-size: 12px;
  line-height: 20px;
  color: #000000;
  border-radius: 4px;
  text-transform: uppercase;
  background: #fff;
  margin-bottom: 5px;
  transition: all 0.3s ease-in-out;
}

#portfolio #portfolio-flters li:hover,
#portfolio #portfolio-flters li.filter-active {
  background: #1aa3ff;
  color: #fff;
}

#portfolio #portfolio-flters li:last-child {
  margin-right: 0;
}

#portfolio .portfolio-wrap {
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.08);
  transition: 0.3s;
}

#portfolio .portfolio-wrap:hover {
  box-shadow: 0px 4px 14px rgba(0, 0, 0, 0.16);
}

#portfolio .portfolio-item {
  position: relative;
  height: 360px;
  overflow: hidden;
}

#portfolio .portfolio-item figure {
  background: #000;
  overflow: hidden;
  height: 240px;
  position: relative;
  border-radius: 4px 4px 0 0;
  margin: 0;
}

#portfolio .portfolio-item figure:hover img {
  opacity: 0.4;
  transition: 0.3s;
}

#portfolio .portfolio-item figure .link-preview,
#portfolio .portfolio-item figure .link-details {
  position: absolute;
  display: inline-block;
  opacity: 0;
  line-height: 1;
  text-align: center;
  width: 36px;
  height: 36px;
  background: #fff;
  border-radius: 50%;
  transition: 0.2s linear;
}

#portfolio .portfolio-item figure .link-preview i,
#portfolio .portfolio-item figure .link-details i {
  padding-top: 6px;
  font-size: 22px;
  color: #333;
}

#portfolio .portfolio-item figure .link-preview:hover,
#portfolio .portfolio-item figure .link-details:hover {
  background: #1aa3ff;
}

#portfolio .portfolio-item figure .link-preview:hover i,
#portfolio .portfolio-item figure .link-details:hover i {
  color: #fff;
}

#portfolio .portfolio-item figure .link-preview {
  left: calc(50% - 38px);
  top: calc(50% - 18px);
}

#portfolio .portfolio-item figure .link-details {
  right: calc(50% - 38px);
  top: calc(50% - 18px);
}

#portfolio .portfolio-item figure:hover .link-preview {
  opacity: 1;
  left: calc(50% - 44px);
}

#portfolio .portfolio-item figure:hover .link-details {
  opacity: 1;
  right: calc(50% - 44px);
}

#portfolio .portfolio-item .portfolio-info {
  background: #fff;
  text-align: center;
  padding: 30px;
  height: 90px;
  border-radius: 0 0 3px 3px;
}

#portfolio .portfolio-item .portfolio-info h4 {
  font-size: 18px;
  line-height: 1px;
  font-weight: 700;
  margin-bottom: 18px;
  padding-bottom: 0;
}

#portfolio .portfolio-item .portfolio-info h4 a {
  color: #333;
}

#portfolio .portfolio-item .portfolio-info h4 a:hover {
  color: #1aa3ff;
}

#portfolio .portfolio-item .portfolio-info p {
  padding: 0;
  margin: 0;
  color: #b8b8b8;
  font-weight: 500;
  font-size: 14px;
  text-transform: uppercase;
}

/*--------------------------------------------------------------
# Footer
--------------------------------------------------------------*/

#footer {
  background: #000;
  padding: 0 0 30px 0;
  color: #eee;
  font-size: 14px;
}

#footer .footer-top {
  background: #111;
  padding: 60px 0 30px 0;
}

#footer .footer-top .footer-info {
  margin-bottom: 30px;
}

#footer .footer-top .footer-info h3 {
  font-size: 34px;
  margin: 0 0 20px 0;
  padding: 2px 0 2px 10px;
  line-height: 1;
  font-family: "Montserrat", sans-serif;
  font-weight: 700;
  letter-spacing: 3px;
  border-left: 4px solid #1aa3ff;
}

#footer .footer-top .footer-info p {
  font-size: 14px;
  line-height: 24px;
  margin-bottom: 0;
  font-family: "Montserrat", sans-serif;
  color: #eee;
}

#footer .footer-top .social-links a {
  font-size: 18px;
  display: inline-block;
  background: #333;
  color: #eee;
  line-height: 1;
  padding: 8px 0;
  margin-right: 4px;
  border-radius: 50%;
  text-align: center;
  width: 36px;
  height: 36px;
  transition: 0.3s;
}

#footer .footer-top .social-links a:hover {
  background: #1aa3ff;
  color: #fff;
}

#footer .footer-top h4 {
  font-size: 14px;
  font-weight: bold;
  color: #fff;
  text-transform: uppercase;
  position: relative;
  padding-bottom: 12px;
}

#footer .footer-top h4::before,
#footer .footer-top h4::after {
  content: '';
  position: absolute;
  left: 0;
  bottom: 0;
  height: 2px;
}

#footer .footer-top h4::before {
  right: 0;
  background: #555;
}

#footer .footer-top h4::after {
  background: #1aa3ff;
  width: 60px;
}

#footer .footer-top .footer-links {
  margin-bottom: 30px;
}

#footer .footer-top .footer-links ul {
  list-style: none;
  padding: 0;
  margin: 0;
}

#footer .footer-top .footer-links ul i {
  padding-right: 8px;
  color: #ddd;
}

#footer .footer-top .footer-links ul li {
  border-bottom: 1px solid #333;
  padding: 10px 0;
}

#footer .footer-top .footer-links ul li:first-child {
  padding-top: 0;
}

#footer .footer-top .footer-links ul a {
  color: #eee;
}

#footer .footer-top .footer-links ul a:hover {
  color: #1aa3ff;
}

#footer .footer-top .footer-contact {
  margin-bottom: 30px;
}

#footer .footer-top .footer-contact p {
  line-height: 26px;
}

#footer .footer-top .footer-newsletter {
  margin-bottom: 30px;
}

#footer .footer-top .footer-newsletter input[type="email"] {
  border: 0;
  padding: 6px 8px;
  width: 65%;
}

#footer .footer-top .footer-newsletter input[type="submit"] {
  background: #1aa3ff;
  border: 0;
  width: 35%;
  padding: 6px 0;
  text-align: center;
  color: #fff;
  transition: 0.3s;
  cursor: pointer;
}

#footer .footer-top .footer-newsletter input[type="submit"]:hover {
  background: #13a456;
}

#footer .copyright {
  text-align: center;
  padding-top: 30px;
}

#footer .credits {
  text-align: center;
  font-size: 13px;
  color: #ddd;
}

/*--------------------------------------------------------------
# Responsive Media Queries
--------------------------------------------------------------*/

@media (min-width: 768px) {
  #contact .contact-address,
  #contact .contact-phone,
  #contact .contact-email {
    padding: 20px 0;
  }

  #contact .contact-phone {
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
  }
}

@media (min-width: 992px) {
  #testimonials .testimonial-item p {
    width: 80%;
  }
}

@media (min-width: 1024px) {
  #header #logo {
    padding-left: 60px;
  }

  #intro p {
    width: 60%;
  }

  /* #intro .carousel-control-prev,
  #intro .carousel-control-next {
    width: 5%;
  } */

  #nav-menu-container {
    padding-right: 60px;
  }
}

@media (max-width: 768px) {
  .back-to-top {
    bottom: 15px;
  }

  #header #logo h1 {
    font-size: 28px;
  }

  #header #logo img {
    max-height: 40px;
  }

  #intro h2 {
    font-size: 28px;
  }

  #nav-menu-container {
    display: none;
  }

  #mobile-nav-toggle {
    display: inline;
  }
}





/* new */
.content {
    background-color: #fafafa;
    width: calc(100% - 100px);
    height: calc(100% - 80px);
    margin: 80px 0 0 100px;
}
/* Start row 1*/
.content .row1 {
    margin: 1% 0 0 0;
    width: 100%;
    height: auto;
    color: #fff;


}

.content .row1 .register {
    background-color: #68ae00;
    padding: 10px;
    float: left;
    width: 90%;
    height: 190px;    
    margin: 3% 1% 1% 0%;
    padding: 1%;
    position: relative;
}



.content .row1 .register:hover {
    background-color: #333
}

.content .row1 .visitors {
    background-color: #68ae00;
    padding: 10px;
    float: left;
    width: 90%;
    height: 190px;
    margin: 3% 1% 1% 1%;
    padding: 1%;
    position: relative;
}



.content .row1 .visitors:hover {
    background-color: #333;
}

.content .row1 .messeges {
    background-color: #68ae00;
    padding: 10px;
    float: left;
    width: 90%;
    height: 190px;
    margin: 3% 2% 1% 1%; 
    padding: 1%;
    position: relative;
}


.content .row1 .messeges:hover {
    background-color: #333
}

/**/

.content .row1 .messeges1 {
    background-color: #68ae00;
    padding: 10px;
    float: left;
    width: 90%;
    height: 190px;
    margin: 3% 2% 1% 0%; 
    padding: 1%;
    position: relative;
    
}



.content .row1 .messeges1:hover {
    background-color: #333
}

/**/

.content .row1 h2 {
    font-size: 25px;
}

.content .row1 p {
    font-size: 25px;
}

.content .row1 span {
    font-size: 15px;
}
/* new */
.button:hover {background-color: #70afff;
 }
 .button{color:#f3f5ff;}


 /* .circular--square {
  border-radius: 50%;
  height:30px;
  width:30px;
  
} */
The style rule above is the shorthand for:

.circular--square {
  border-top-left-radius: 50% 50%;
  border-top-right-radius: 50% 50%;
  border-bottom-right-radius: 50% 50%;
  border-bottom-left-radius: 50% 50%;
}
 .pe-slide {
    margin-bottom: 25px;
    position: relative;

    }
    .product-description {
    background-color: #F7F7F7;
    border-top: 1px solid #EFEFEF;
    padding: -100px 20px;
    color: #797979;
    }
    .product-option h3 {
    font-size: 16px;
    font-weight: bold;
    margin-bottom: -12px;
    padding: -30px;
}

 h3{
 margin-bottom: 10px;  
}

.wsk-cp-product{
  background:#fff;
  padding:15px;
  border-radius:6px;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  position:relative;
  margin:20px auto;
  
}
.wsk-cp-img{
  position:absolute;
  top:5px;
  left:50%;
  transform:translate(-50%);
  -webkit-transform:translate(-50%);
  -ms-transform:translate(-50%);
  -moz-transform:translate(-50%);
  -o-transform:translate(-50%);
  -khtml-transform:translate(-50%);
  width: 100%;
  
  padding: 15px;
  transition: all 0.2s ease-in-out;
}
.wsk-cp-img img{
  width:100%;

  transition: all 0.2s ease-in-out;
  border-radius:6px;
}
.wsk-cp-product:hover .wsk-cp-img{
  top:-40px;
}
.wsk-cp-product:hover .wsk-cp-img img{
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
}
.wsk-cp-text{
  padding-top:150%;
}
.wsk-cp-text .category{
  text-align:center;
  font-size:12px;
  font-weight:bold;
  padding:5px;
  margin-bottom:45px;
  position:relative;
  transition: all 0.2s ease-in-out;
  font-family: 'Lemonada', cursive;
}
.wsk-cp-text .category > *{
  position:absolute;
  top:50%;
  left:50%;
  transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
  -moz-transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
  -o-transform: translate(-50%,-50%);
  -khtml-transform: translate(-50%,-50%);
    
}
.wsk-cp-text .category > span{
  padding: 12px 30px;
  border: 1px solid #313131;
  background:#212121;
  color:#fff;
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
  border-radius:27px;
  transition: all 0.05s ease-in-out;
  
}
.wsk-cp-product:hover .wsk-cp-text .category > span{
  border-color:#ddd;
  box-shadow: none;
  padding: 11px 28px;
}
.wsk-cp-product:hover .wsk-cp-text .category{
  margin-top: 0px;
}
.wsk-cp-text .title-product{
  text-align:center;
}
.wsk-cp-text .title-product h3{
  font-size:20px;
  font-weight:bold;
  margin:15px auto;
  overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  width:100%;
}
.wsk-cp-text .description-prod p{
  margin:0;
}

/* 
.wsk-cp-product{
  background:#fff;
  padding:15px;
  border-radius:6px;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  position:relative;
  margin:20px auto;
  height: 600px;
  width: ;
 }
.wsk-cp-img{
  position:absolute;
  top:5px;
  left:50%;
  transform:translate(-50%);
  -webkit-transform:translate(-50%);
  -ms-transform:translate(-50%);
  -moz-transform:translate(-50%);
  -o-transform:translate(-50%);
  -khtml-transform:translate(-50%);
  width: 100%;
 
  
  
  padding: 5px;
  transition: all 0.2s ease-in-out;
}
.wsk-cp-img img{
  width:100%;
height: 270px;
  transition: all 0.2s ease-in-out;
  border-radius:6px;
}
.wsk-cp-product:hover .wsk-cp-img{
  top:-40px;
}
.wsk-cp-product:hover .wsk-cp-img img{
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
}
.wsk-cp-text{
  padding-top:150%;
}
.wsk-cp-text .category{
  text-align:center;
  font-size:12px;
  font-weight:bold;
  padding:5px;
  margin-bottom:45px;
  position:relative;
  transition: all 0.2s ease-in-out;
  font-family: 'Lemonada', cursive;

}
.wsk-cp-text .category > *{
  position:absolute;
  top:50%;
  left:50%;
  transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
  -moz-transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
  -o-transform: translate(-50%,-50%);
  -khtml-transform: translate(-50%,-50%);
    
}
.wsk-cp-text .category > span{
  padding: 12px 30px;
  border: 1px solid #313131;
  background:#212121;
  color:#fff;
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
  border-radius:27px;
  transition: all 0.05s ease-in-out;
  
}
.wsk-cp-product:hover .wsk-cp-text .category > span{
  border-color:#ddd;
  box-shadow: none;
  padding: 11px 28px;
}
.wsk-cp-product:hover .wsk-cp-text .category{
  margin-top: 0px;
}
.wsk-cp-text .title-product{
  text-align:center;
}
.wsk-cp-text .title-product h3{
  font-size:20px;
  font-weight:bold;
  margin:15px auto;
  overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  width:100%;
}
.wsk-cp-text .description-prod p{
  margin:0;
} */
/* Truncate */
.wsk-cp-text .description-prod {
  text-align:center;
  width: 100%;
  height:220px;
  overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  margin-bottom:15px;
}
.card-footer{
  padding: 25px 0 5px;
  border-top: 1px solid #ddd;
}
.card-footer:after, .card-footer:before{
  content:'';
  display:table;
}
.card-footer:after{
  clear:both;
}

.card-footer .wcf-left{
  float:left;
  
}

.card-footer .wcf-right{
  float:right;
}

.price{
  font-size:18px;
  font-weight:bold;
}

a.buy-btn{
  display:block;
  color:#212121;
  text-align:center;
  font-size: 18px;
  width:35px;
  height:35px;
  line-height:35px;
  border-radius:50%;
  border:1px solid #212121;
  transition: all 0.2s ease-in-out;
}
a.buy-btn:hover , a.buy-btn:active, a.buy-btn:focus{
  border-color: #FF9800;
  background: #FF9800;
  color: #fff;
  text-decoration:none;
}
.wsk-btn{
  display:inline-block;
  color:#212121;
  text-align:center;
  font-size: 18px;
  transition: all 0.2s ease-in-out;
  border-color: #FF9800;
  background: #FF9800;
  padding:12px 30px;
  border-radius:27px;
  margin: 0 5px;
}
.wsk-btn:hover, .wsk-btn:focus, .wsk-btn:active{
  text-decoration:none;
  color:#fff;
}  
.red{
  color:#F44336;
  font-size:22px;
  display:inline-block;
  margin: 0 5px;
}
@media  screen and (max-width: 991px) {
  .wsk-cp-product{
    margin:40px auto;
  }
  .wsk-cp-product .wsk-cp-img{
  top:-40px;
}
.wsk-cp-product .wsk-cp-img img{
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
}
  .wsk-cp-product .wsk-cp-text .category > span{
  border-color:#ddd;
  box-shadow: none;
  padding: 11px 28px;
}
.wsk-cp-product .wsk-cp-text .category{
  margin-top: 0px;
}
a.buy-btn{
  border-color: #FF9800;
  background: #FF9800;
  color: #fff;
}
}

/* contact button  july 25*/
.btn-teo-caption2 {
  width: 200px;
  border-radius: 5% !IMPORTANT;
  border: 2px solid #106470;
  margin-top: 6px;
  margin-right: 21%;
  background-color: #1e90ff ;
  font-weight: bold;
  color: white;
  
 }
.btn-teo-caption2:hover {
  background-color: white;
	color: #106470;
  border: 2px solid #106470;
	transition-property: ease 1s;
}
#caption2-button {
  font-family: "Open Sans", sans-serif;
  font-size: 2em;
  font-weight: 300;
  position: absolute;
  bottom: -10px;
  left: 40%;
  color: black;
  text-decoration: none;
}
.btn-teo-caption1 {
  width: 100px;
  border-radius: 50% !IMPORTANT;
  border: 2px solid #106470;
  margin-top: 6px;
  margin-right: 21%;
  background-color: #F9EBEA ;
  font-weight: bold;
  color: black;
 }
.btn-teo-caption1:hover {
  background-color: white;
	color: #106470;
  border: 2px solid #106470;
	transition-property: ease 1s;
}
#caption1-button {
  font-family: "Open Sans", sans-serif;
  font-size: 2em;
  font-weight: 300;
  position: absolute;
  bottom: -10px;
  left: 40%;
  color: black;
  text-decoration: none;
}
.box
		{
		 width:100%;
		 max-width:95%;
		 background-color:#f9f9f0;
		 border:1px solid #ccc;
		 border-radius:5px;
		 padding:16px;
		 margin:0 auto;
		 height:100%;
		  
  border-collapse: collapse; 
		 
		}
		input.parsley-success,
		select.parsley-success,
		textarea.parsley-success {
		  color: #468847;
		  background-color: #DFF0D8;
		  border: 1px solid #D6E9C6;
		}
	  

</style>
@endsection

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/js/swiper.jquery.min.js"></script>
	<script src="{{ asset('js/homepage.js') }}"></script>
	
  <script src="css/lib/jquery/jquery.min.js"></script>
  <script src="css/lib/jquery/jquery-migrate.min.js"></script>
  <script src="css/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="css/lib/easing/easing.min.js"></script>
  <script src="css/lib/superfish/hoverIntent.js"></script>
  <script src="css/lib/superfish/superfish.min.js"></script>
  <script src="css/lib/wow/wow.min.js"></script>
  <script src="css/lib/waypoints/waypoints.min.js"></script>
  <script src="css/lib/counterup/counterup.min.js"></script>
   <script src="css/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="css/lib/touchSwipe/jquery.touchSwipe.min.js"></script>



  

   
  <!-- Contact Form JavaScript File -->
  
  <!-- Template Main Javascript File -->
  <script src="css/js/main.js"></script>


 


 
@endsection

@section('content')
    
 
 
 
     
     


 
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="height:500px;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox" style="height:500px;width:100%;">
      <div class="item active">
        <img src="{{asset('images/banners/photobook.jpg')}}" style="width:100%;height:500px;">
      </div>
      <div class="item">
        <img src="{{asset('images/banners/dairy.jpg')}}" style="width:100%;height:500px;">
      </div>
      <div class="item">
        <img src="{{asset('images/banners/pic2.jpg')}}" style="width:100%;height:500px;">
      </div>
      <div class="item">
        <img src="{{asset('images/banners/slider4.jpg')}}" style="width:100%;height:500px;">
      </div>
      <!-- <div class="item">
        <img src="{{asset('images/pro4.jpg')}}" style="width:100%;height:500px;">
      </div>
      <div class="item">
        <img src="{{asset('images/pro5.jpg')}}" style="width:100%;height:500px;">
      </div>
    </div> -->

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
      





	
	<div class="container" id="aboutus" style="margin-top: 5vh; margin-bottom: 25vh;">
		<div class="row">

      <div class="page-header">
				
        <h2 class="text-center" id="box"> Welcome to <b>SHIVPICS </b></h2>
      </div>
    <center>  <b>Contact Details</b><br> </p>
      <i class="glyphicon glyphicon-phone blink " style="font-size:18px; color:skyblue;"> </i> <b> </b> <a href="tel: +91-8983691455" style="color:blue"> +91-8983691455 </a> 

        
      &nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-envelope blink " style="font-size:18px; color:skyblue;"></i>&nbsp; <b> </b>  <a href="mailto:shivanshcreation04@gmail.com" style="color:blue">shivanshcreation04@gmail.com </a>  <br>
  <span style="font-family: Lato, sans-serif; font-size:29px;"><b>Direct Whatsapp order      <i class="fa fa-hand-o-right" aria-hidden="true"></i> </b></span>
        &nbsp;&nbsp;&nbsp; <a href="https://wa.me/8983691455 "><div  class="btn btn-default text-center btn-teo-caption1" style="margin:20px;"> <i class="fa fa-whatsapp" style="font-size:48px;color:green"></i></a>

		
				
			</div>
		</div><hr style="border-color: gray">

<body style="background-color: #fff;font-family: Lato, sans-serif;">

  <main id="main">

   
    <!-- <section id="portfolio"  class="section-bg" ></section> -->
    {{-- <section id="portfolio"  class="section-bg" style="background-color:#f0f0f0" > --}}
    <header class="section-header" >
           <h3 style= "font-family:Gungsuh">Our Products</h3>
        </header><br>


        
 
 {{-- <div class="shell">
  <div class="container">
    <div class="row">
      <div class="col-md-3"  >
        <div class="wsk-cp-product">
          <div class="wsk-cp-img">
            <img src="images/spiral.jpg" alt="Product" class="img-responsive" />
          </div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral Register</span>
            </div>
            <div class="title-product">
             </div>
            <div class="description-prod">
             <ul class="list-group">
                     <li class="list-group-item">Personalized Photo  </li>
                     <li class="list-group-item">A4 Size  </li>

                    <li class="list-group-item">Plain Paper </li>
                     <li class="list-group-item">Ruled Paper</li>
              </ul>
           </div>
           </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img"><img src="images/behappy.jpg" alt="Product" class="img-responsive" /></div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral Dairy</span>
            </div>
            <div class="title-product">
             </div>
            <div class="description-prod">
            <ul class="list-group">
            <li class="list-group-item">Personalized Photo </li>
                      <li class="list-group-item">A5 Size  </li>
                      <li class="list-group-item">Plain Paper </li>
                      <li class="list-group-item">Ruled Paper</li>              </ul>
           </div>
           </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img"><img  src="images/calender.jpg" alt="Product" class="img-responsive" /></div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Calender</span>
            </div>
            <div class="title-product">
             </div>
            <div class="description-prod">
            <ul class="list-group">
                     <li class="list-group-item">Landscape & Portrait Wall Calender- A4 Size </li>
                     <li class="list-group-item"> Desk Calender- A5 Size </li>
                    <li class="list-group-item"> Poster Calender- A4 size </li>



                   </ul>
           </div>
             </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img"><img  src="images/spiralpb.jpg" alt="Product" class="img-responsive" /></div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral PhotoBooks</span>
            </div>
            <div class="title-product">
             </div>
            <div class="description-prod">
            <ul class="list-group">
                     <li class="list-group-item">A4 & A5 Size Photo Album </li>
                    <li class="list-group-item">5x7 & 4x6  Photo Size   </li>
                    <li class="list-group-item">20 Pages, 32 Pages </li>
                    <li class="list-group-item">(Soft Cover)</li>



                   </ul>
           </div>
             </div>
        </div>
      </div>
    </div>
     </div>
</div> --}}
<div class="shell">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img">
            <img src="images/spiral.jpg" style="height:350px" alt="Product" class="img-responsive" />
          </div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral Register</span>
            </div>
            <div class="title-product">
            {{-- <h3> </h3> --}}
            </div>
            <div class="description-prod">
             <ul class="list-group">
              <li class="list-group-item">Personalized Photo  </li>
              <li class="list-group-item">A4 Size  </li>
             <li class="list-group-item">Plain Paper </li>
              <li class="list-group-item">Ruled Paper</li>
              </ul>
           </div>
           </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img"><img src="images/behappy.jpg" style="height:350px" alt="Product" class="img-responsive" /></div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral Dairy</span>
            </div>
            <div class="title-product">
              <h3> </h3>
            </div>
            <div class="description-prod">
            <ul class="list-group">
              <li class="list-group-item">A5 Size  </li>
              <li class="list-group-item">Plain Paper </li>
              <li class="list-group-item">Ruled Paper</li>  
             </ul>
           </div>
           </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img"><img src="images/calender.jpg"style="height:350px" alt="Product" class="img-responsive" /></div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral Calender</span>
            </div>
            <div class="title-product">
              <h3></h3>
            </div>
            <div class="description-prod">
            <ul class="list-group">
              <li class="list-group-item">Landscape & Portrait Wall Calender- A4 Size </li>
              <li class="list-group-item"> Desk Calender- A5 Size </li>
             <li class="list-group-item"> Poster Calender- A4 size </li>
                   </ul>
           </div>
             </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="wsk-cp-product">
          <div class="wsk-cp-img"><img src="images/spiralpb.jpg" style="height:350px;" alt="Product" class="img-responsive" /></div>
          <div class="wsk-cp-text">
            <div class="category">
              <span style="font-size:20px;">Spiral PhotoBooks </span>
            </div>
            <div class="title-product">
              <h3> </h3>
            </div>
            <div class="description-prod">
            <ul class="list-group">
              <li class="list-group-item">A4 & A5 Size Photo Album </li>
              <li class="list-group-item">5x7 & 4x6  Photo Size   </li>
              <li class="list-group-item">20 Pages, 32 Pages </li>
              <li class="list-group-item">(Soft Cover)</li>
             
              </ul>
           </div>

          </div>
        </div>
      </div>
    </div>
     </div>
</div>
</section>

 
     
 <section id="portfolio"  >
<header class="section-header">
          <h3 style= "font-family:Gungsuh">Order</h3>
        </header> 
        
        <div class="container" style="width: 100%" >
          <div class="box" style="background-color:#C568E5 ; width:100%">
           <div class="box" >
             
            <form id="validate_form"  method="post" action="{{ url('/order') }}" name="sentMessage">
              {{ csrf_field() }}
                  <div class="control-group form-group">
    
                <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <label style="margin-top:10px; font-size:18px;"><i class="glyphicon glyphicon-pencil"style="margin-right:9px"></i>Full Name:</label>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
                  <input type="text" class="form-control" id="name" placeholder="Enter Name" required data-validation-required-message="Please enter your name." name="name" required data-parsley-pattern="[a-zA-Z ]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup">
                  <p class="help-block"></p> 
                  </div>
                </div>
              </div>
    
              <div class="control-group form-group">
                <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                  <label style="margin-top:10px;font-size:18px;"><i class="glyphicon glyphicon-phone"style="margin-right:10px"> </i>Phone Number</label>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
                  <input type="tel" class="form-control"     id="phone" required data-validation-required-message="Please enter your phone number." name="phone" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup">
                  <p class="help-block"></p>  </div>
                  <!-- <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number." name="cnumber" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup"> -->
                </div>
              </div> 
              <div class="control-group form-group">
                <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                  <label style="margin-top:10px;font-size:18px;"> <i class="glyphicon glyphicon-envelope"style="margin-right:10px"> </i>Email</label>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address." name="email" placeholder="Email" required data-parsley-type="email"  data-parsley-trigger="keyup">
                  <p class="help-block"></p></div>
                </div>
              </div>

              <div class="control-group form-group">
                <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                  <label style="margin-top:10px;font-size:18px;"> <i class="glyphicon glyphicon-th-list"style="margin-right:10px"> </i>Order Type</label>
                  </div>  
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <select id="catg" name="catg"  class="form-control fa" style="margin-top:px; " required>

                      <option  value="  ">Select Any One</option>
                      <option style="height:60px;"   value="E-Commerce Solution"><i >&#xf2b9;</i> &nbsp;Spiral Register</option>
                      <option style="height:60px;"  value="E-Learning Platform"><i >&#xf02d;</i> &nbsp;Spiral Dairy</option>
                      <option style="height:60px;"  value="Billing & Order Solution"><i >&#xf073;</i> &nbsp; Calender</option>
                      <option style="height:60px;"  value=" Health Care"><i >&#xf1c5;</i> &nbsp;Spiral Photobook</option>
                     
        
                      </select>                  
                      <p class="help-block"></p></div>
                </div>
              </div>


              <div class="control-group form-group">
                <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                  <label style="margin-top:10px;font-size:18px;"><i class="fa fa-address-card"style="margin-right:10px"> </i>Shipping Address</label>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea rows="2" cols="20" class="form-control" id="shipad" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="shipad" placeholder="shipping address"></textarea><br>
                  <p class="help-block"></div></div></div>

                    <div class="control-group form-group">
                      <div class="controls">
                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                        <label style="margin-top:10px;font-size:18px;"><i class="glyphicon glyphicon-check"style="margin-right:10px"> </i>Sub-type, size & Your Requirement</label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <textarea rows="2" cols="20" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="message" placeholder="Message"></textarea><br>
                        <p class="help-block"></div></div></div>
              
              <div id="success"></div>
             <center>   <button type="submit" class="btn btn-default text-center btn-teo-caption2" id="sendMessageButton" style="height:50px;width:20 0px; ">Order </button></center> 
            </form> 
      
      
                      
     </div></div></div>
    </section> 

  </div>   </main>

@endsection
