@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/justifiedGallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lg-transitions.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/articles/showarticle.css') }}" />
	<style>
				@media (min-width: 800px) {
#feedback {
			float: right;
			position: fixed;
			top: calc(50% - 47px);
			left: 0;
		}

	}

	@media (max-width: 799px) {
		#feedback {
			float: right;
			position: fixed;
			top: calc(90% - 50px);
			left: 0;
		}
	}
		#feedback a {
			background: #FF4500;
			border-radius: 5px 0 0 5px;
			box-shadow: 0 0 3px rgba(0, 0, 0, .3);
			border: 3px solid #fff;
			border-left: 0;
			display: block;
			padding: 20px 12px;
			transition: all .2s ease-in-out;
		}

		#feedback a:hover {
			padding-left: 20px;
		}
		 
}
#box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
  /* animation: appear 4s infinite; */
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}
  display: none;{
  top: 100%;
  left: 0;
  z-index: 99;
}


</style>
@endsection

@section('js')
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
	<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('js/articles/showarticle.js') }}"></script>

	
@endsection

@section('content')
<div class="container" style="margin-top: 0em;">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-5" >
 					<h2 id='box'>
           Dealership
				    </h2>
			 			</div>
		</div>
 <BR>

	<div class="container-fluid" style="background-color: #ebebeb">
		<div class="row">
			<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<form class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input-group">
									<div class="input-group-addon">Filter With Tags : </div>
									<input type="text" class="form-control" id="searchArticleInput" placeholder="Enter Tag"/>
									<div class="input-group-btn">
										<btn class="btn btn-primary btn-raised" type="submit" id="searchArticles" data-path="{{ url('/userblogs/gallery/get/') }}" style="margin-bottom: 0;">Search Articles</btn>
									</div>
								</div>
							</form>
						</div>
						<div class="row article-gallery">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div id="gallery" data-next-page="{{ url('/userblogs/get') }}">

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection
