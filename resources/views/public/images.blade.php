@extends('layouts.app')

@section('css')
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<link rel="stylesheet" href="{{ asset('css/justifiedGallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lg-transitions.min.css') }}" />
	
@endsection

@section('js')
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
	<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('js/images.js') }}"></script>
@endsection
<style>

input[type=text]:focus {
border-color: black;
}
#box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
  /* animation: appear 4s infinite; */
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}
  
}
.well {
    min-height: 20px;
    padding: 29px;
    margin-bottom: 20px;
    background-color:# ;
    border: 2px solid  #58c123;
    border-radius: 16px;
    -webkit-box-shadow: inset 0 3px 1px rgba(0,0,0,0.05);
    box-shadow: inset 0 3px 1px rgba(0,0,0,0.05);
}
/* .zoom {
  padding: 50px;
  transition: transform .9s;
}
.well:hover {
	 
  transform: scale(0.9, 0.9);
  box-shadow: 3px 3px 25px 10px rgba(0,0,0,0.25), 
    -2px -2px 25px 15px rgba(0,0,0,0.22);
}
 */

.img-hover-zoom {
   
  overflow: hidden; /* [1.2] Hide the overflowing of child elements */
}

/* [2] Transition property for smooth transformation of images */
.img-hover-zoom img {
  transition: transform .9s ease;
}

/* [3] Finally, transforming the image when container gets hovered */
.img-hover-zoom:hover img {
  transform: scale(1.1);
}
 



 

</style>
@section('content')
<div class="container" style="margin-top: 6em;">
		<div class="row">
			<div class="col-xs-12 col-xs-offset-0" >
 				<center>	<h2 id='box'>
                    Images
				    </h2></center>
			 			</div>
		</div>
<br> 
<div class="container-fluid ">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row"style="top-padding:5px">
						<!-- <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="input-group" style="width:60%;margin:auto;">
							
								<input type="text" class="form-control" id="searchImageInput" placeholder="Enter Tag"/>
								<div class="input-group-btn">
									<btn class="btn btn-primary btn-raised" type="submit" id="searchImages" data-path="{{ url('/images/gallery/get/') }}" style="margin-bottom: 0;background-color:black;border-color:black;  width: 2.5em;  height: 2.5em;"><span class="glyphicon glyphicon-search"></span></btn>
								</div>
							</div>
						</form> -->
					</div>
					<!-- <div class="row image-gallery">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> -->
							<!-- <div id="gallery" data-next-page="{{ url('/images/gallery/get') }}">
							</div> -->
						@foreach($images as $image)

						
						<div class="image-gallery ">
						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 img-hover-zoom">

						<img   style="border-radius: 9px;border:2px solid #000; height:300px; width:100% " src="{{ asset(''. $image->path) }}" alt="Product" class="img-responsive">&nbsp;&nbsp;&nbsp;
 
						</div></div>

						@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 
@endsection
