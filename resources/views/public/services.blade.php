@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<style>
	blockquote {
	    padding: 11px 22px;
	    margin: 0 0 22px;
	    font-size: 20px;
	    border-left: 5px solid black;
	}

  #box {
  margin: 30px auto 0 auto;
  animation: appear 4s infinite;
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}
</style>
@endsection
<!-- <script>
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script> -->
@section('js')

@endsection

@section('content')
	<div class="container" style="margin-top: 10em;">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1" >
				<blockquote style="color:black;">
					<h2 id='box'>
                    Services
				    </h2>
				</blockquote>
			</div>
		</div>
  <!-- <div class="container" style="margin-top: 5em; margin-bottom: 5em;">
   <img class="img-fluid rounded mb-4" src="http://placehold.it/1200x300" alt="" style="margin:auto;">
  </div> -->

	<div class="w3-container w3-center w3-animate-top">
	  <h1 style="color:#1B3057;">Analysing your needs to provide best products for your BUSINESS</h1>
	</div>
  <div class="container" style="margin-top: 5em; margin-bottom: 5em;">
    <div class="row" style="padding-top:10px;">
       <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <a href="#">
           <img class="img-responsive" src="{{asset('images/services/BeFunky Collage5.jpg')}}" alt="" style="width:500px;">
         </a>
       </div>
       <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
				 <h3>User Interface</h3>
         <p>We provide user friendly products along with the necessary mobile compatibility to succeed in all walks of life.
      </div>
     </div>
		 <hr>

     <div class="row" style="padding-top:10px;">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
         <a href="#">
            <img class="img-responsive" src="{{asset('images/services/performanceIIUJH.jpg')}}" alt="" style="width:500px;">
          </a>
 				 <h1 style="font-size:50px;text-align:center;">
   			 		<a style="color:#0F0638;" href="" class="typewrite" data-period="4000" data-type='[ "PERFORMANCE" ]'>
     					<span class="wrap"></span>
   					</a>
 					</h1>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
          <h3>Performance</h3>
          <p>Our products have high and durable performance including the loading time of websites which is faster saving a large amount of time and consistent manual efforts.
          </p>
        </div>
				
      </div>
			<hr>

      <div class="row" style="padding-top:10px;">
				<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
           <a href="#">
            <img class="img-responsive" src="{{asset('images/services/BeFunky Collage2.jpg')}}" alt="" style="width:500px;">
          </a>
 				</div>
         <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
           <h3>Low maintenance cost</h3>
           <p>We try to ensure and provide a dynamic functionality in order to save further maintenance costs and efforts.</p>
         </div>
       </div>
			 <hr>

       <div class="row" style="padding-top:10px;">
         	<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
 	        <a href="#">
 	           <img class="img-responsive" src="{{asset('images/services/BeFunky.jpg')}}" alt="" style="width:500px;">
 	         </a>
 	       </div>
          <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <h3>Mass communication</h3>
            <p>In today’s ultra-modern world of mass digitization, it is of paramount importance to boost your business with the help of social media. With regards to this, we provide extensive social media support to our customers with the sole objective of helping them expand their business. We broadcast through all social networking sites like Facebook, Twitter, YouTube and Instagram. Additionally, we provide automatic mailing and messaging services to our clients so that they can rest assured knowing that this would definitely result in a prompt and fast-paced delivery routine.
            </p>
          </div>
				
        </div>
				<hr>

        <div class="row" style="padding-top:10px;">
					<div class="col-lg-6">
					 <a href="#">
							<img class="img-responsive" src="{{asset('images/services/BeFunky Collage4.jpg')}}" alt="" style="width:500px;">
						</a>
					 <h1 style="font-size:50px;text-align:center;">
							<a style="color:#0F0638;" href="" class="typewrite" data-period="4000" data-type='[ "BACKUP" ]'>
								<span class="wrap"></span>
							</a>
						</h1>
					</div>

           <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">
             <h3>Backup</h3>
             <p>Having our own server prevents data loss and minimizes the risk of corruption and collision.</p>
           </div>
         </div>
				 <hr>
				 <div class="row" style="padding-top:10px;">
         		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
 		         <a href="#">
 		           <img class="img-responsive" src="{{asset('images/services/BeFunky Collage420.jpg')}}" alt="" style="width:500px;">
 		         </a>
 						 <h1 style="font-size:50px;text-align:center;">
 		  			 		<a style="color:#0F0638;" href="" class="typewrite" data-period="4000" data-type='[ "SECURITY" ]'>
 		    					<span class="wrap"></span>
 		  					</a>
 							</h1>
               
 		       </div>
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 ">
              <h3>Security</h3>
              <p>OTP: One-time passwords can be a tremendous benefit to security. We provide OTPs as they are more secure.</br>
Password: Your data is completely secure even at the server side as all the passwords are in an encrypted format.	</br>
SSL: SSL (Secure Socket Layer) is a standard security protocol used for the purpose of establishing encrypted links between a web server and a browser in an online communication. We also use SSL as a standard practice while designing our website and to maintain confidential information.
              </p>
            </div>
				
          </div>

          <div class="row" style="padding-top:10px;">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
							<a href="#">
								<img class="img-responsive" src="{{asset('images/services/BeFunky Collage61.jpg')}}" alt="" style="width:500px;">
							</a>
							<h1 style="font-size:50px;text-align:center;">
								 <a style="color:#0F0638;" href="" class="typewrite" data-period="4000" data-type='[ "DOMAIN NAME AND HOSTING" ]'>
									 <span class="wrap"></span>
								 </a>
							 </h1>
						</div>
             <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
               <h3>Domain name and hosting</h3>
               <p>We do provide third-party domain name and hosting services.
               </p>
             </div>
           </div>
					 <hr>
           <div class="row" style="padding-top:10px;">
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
 			        <a href="#">
 			           <img class="img-responsive" src="{{asset('images/services/BeFunky_Collage.jpg')}}" alt="" style="width:500px;">
 			         </a>
 							 <h1 style="font-size:50px;text-align:center;">
 			  			 		<a style="color:#0F0638;" href="" class="typewrite" data-period="4000" data-type='[ "POC (PROOF OF CONCERN)" ]'>
 			    					<span class="wrap"></span>
 			  					</a>
 								</h1>
 			       </div>
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <h3>POC (Proof of Concept)</h3>
                <p>We provide POC to our clients to enable risk-free and fool-proof communication.</p>
              </div>
							
            </div>
						 <hr>
            <div class="row" style="padding-top:10px;">
							<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
							<a href="#">
									<img class="img-responsive" src="{{asset('images/services/BeFunky Collage82.jpg')}}" alt="" style="width:500px;">
								</a>
								</div>
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                 <h3>Content writing and logo design</h3>
                 <p>Other than software products, we also provide relevant content for your website. We have special and dedicated content writers for the purpose of brochure creation, advertisement content and blog writing. We also indulge in professional logo designing services, as we believe that the presentation of a product is as important as the overall product itself.
</p>
               </div>
             </div>
						  <hr>
             <div class="row" style="padding-top:10px;">
                	<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
 				         <a href="#">
 				           <img class="img-responsive" src="{{asset('images/services/BeFunky Collage8.jpg')}}" alt="" style="width:500px;">
 				         </a>
 								 <h1 style="font-size:50px;text-align:center;">
 				  			 		<a style="color:#0F0638;" href="" class="typewrite" data-period="1000" data-type='[ "BUSINESS IDEAS" ]'>
 				    					<span class="wrap"></span>
 				  					</a>
 									</h1>
 				       </div>
               <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <h3>Business ideas</h3>
                  <p>We share ideas for expanding your business.
You can offer discount schemes, promo codes to your customer through websites. Dynamic news section will definitely keep the customer constantly updated about new things and aspects in the market, thereby enhancing the overall business. The customer also receives messages and alerts via email with respect to the above. So, all in all, we help to reduce redundancy of tasks and jobs, thereby reducing usage of time and wastage of consistent manual efforts. We do provide unique ideas for advertising your product with the help of possible social media channels, thereby expanding your business.
                  </p>
                </div>
							
              </div>


	</div>


@endsection
