@extends('layouts.app')

@section('css')
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<link rel="stylesheet" href="{{ asset('css/justifiedGallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lg-transitions.min.css') }}" />
	
	<style>
	input[type=text]:focus {
	border-color: black;
	}
	@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}
	</style>
@endsection

@section('js')
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
	<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('js/videos.js') }}"></script>
	<style>
		blockquote {
	    padding: 11px 22px;
	    margin: 0 0 22px;
	    font-size: 20px;
	    border-left: 5px solid black;
	}

	#box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
}

.justified-gallery>a>.caption, .justified-gallery>div>.caption {
    display: none;
    position: absolute;
    bottom: 0;
    padding: 20px;
    background-color: #51595f;
    left: 0;
    right: 0;
    margin: 0;
    color: #fff;
    font-size: 21px;
    font-weight: 300;
    font-family: sans-serif;
	text-align:center;
	
}
element.style {
    width: 524px;
    height: 300px;
    margin-left: -262px;
    margin-top: -147.5px;
	border-style:groove;

}
 

	</style>

@endsection


@section('content')
<div class="container" style="margin-top: 6em;">
		<div class="row">
			<div class="col-xs-12 col-xs-offset-0" >
			<center>	<h2 id='box'>
                    Videos
				    </h2></center>
		 
			</div>
		</div>
<!-- <br><center><h2>Videos </h2></center> -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row"style="top-padding:5px">
							<form class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="input-group" style="width:250px;margin:auto;">
									<!-- <div class="input-group-addon">Filter With Tags : </div> -->
									<input type="text" class="form-control" id="searchVideoInput" placeholder="Enter Tag"/>
									<div class="input-group-btn">
										<btn class="btn btn-primary btn-raised" type="submit" id="searchVideos" data-path="{{ url('/videos/gallery/get/') }}" style="margin-bottom: 0;background-color:black;"><span class="glyphicon glyphicon-search"></span></btn>
									</div>
								</div>
							</form><br><br><br>
						</div>
						<div class="row image-gallery">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div id="gallery"    data-next-page="{{ url('/videos/gallery/get') }}">&nbsp;&nbsp;&nbsp;
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		</div>

	</div><br> 
@endsection
