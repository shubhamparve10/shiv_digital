 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NeetaTech Requirement</title>
  
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	



    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">



<script src="http://parsleyjs.org/dist/parsley.js"></script>
<script>$(document).ready(function(){
    $('#validate_form').parsley();

    
});
</script>

 

	
	<style>
		.box
		{
		 width:100%;
		 max-width:95%;
		 background-color:#f9f9f0;
		 border:1px solid #ccc;
		 border-radius:5px;
		 padding:16px;
		 margin:0 auto;
		 height:100%;
		  
  border-collapse: collapse; 
		 
		}
		input.parsley-success,
		select.parsley-success,
		textarea.parsley-success {
		  color: #468847;
		  background-color: #DFF0D8;
		  border: 1px solid #D6E9C6;
		}
	  
		input.parsley-error,
		select.parsley-error,
		textarea.parsley-error {
		  color: #B94A48;
		  background-color: #F2DEDE;
		  border: 1px solid #EED3D7;
		}
	  
		.parsley-errors-list {
		  margin: 2px 0 3px;
		  padding: 0;
		  list-style-type: none;
		  font-size: 0.9em;
		  line-height: 0.9em;
		  opacity: 0;
	  
		  transition: all .3s ease-in;
		  -o-transition: all .3s ease-in;
		  -moz-transition: all .3s ease-in;
		  -webkit-transition: all .3s ease-in;
		}
	  
		.parsley-errors-list.filled {
		  opacity: 1;
		}
	  
		.parsley-type, .parsley-required, .parsley-equalto, .parsley-pattern, .parsley-length{
		 color:#ff0000;
		}

        body {
 
 background-color: #71C2EE;
}

.parsley-errors-list {
    margin: 2px 0 3px;
    padding: 0;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }

  .parsley-type, .parsley-required, .parsley-equalto, .parsley-pattern, .parsley-length{
   color:#ff0000;
  }

  .blink {
  animation: blinker 1s linear infinite;
}

@keyframes blinker {  
  50% { opacity: 0.0; }
}
.fa option {

font-weight: 900;
}
   </style>
      
      
	  
	  </head>

	  <body >
		<div class="container">
			 
		<span><h3 align="center"><B style="color:white;"> NeetaTech</B> </h3>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-envelope blink  col-md-offset-4" style="font-size:13px;"> </i> sales@neetatech.com 
	
	<i class="glyphicon glyphicon-phone blink" style="font-size:13px;">  </i>+91 8793363725 </span>
			
			<h3 align="center"><B style="color:white;"> Business Requirement</B> </h3>
			 
			<div class="box" >
				<form id="validate_form"  method="post" action="{{ url('/requirement/insert') }}" name="sentMessage" >
				{{ csrf_field() }}
	
					 
					<div class="row">
					 <div class="col-xs-6">
					  <div class="form-group">
                      <i class="glyphicon glyphicon-pencil"></i> &nbsp;<label>First Name</label>
					   <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Enter First Name" required data-parsley-pattern="[a-zA-Z]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup" />
					  </div>
					 </div>
					 <div class="col-xs-6">
					  <div class="form-group">
                      <i class="glyphicon glyphicon-pencil"></i>&nbsp;<label>Last Name</label>
					   <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Enter Last Name" required data-parsley-pattern="[a-zA-Z]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup" />
					  </div>
					 </div>
					</div>
					<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
                        <span class="glyphicon glyphicon-envelope"></span>&nbsp; <label>Email</label> 
					 <input type="text" name="email" id="email" class="form-control" placeholder="Email" required data-parsley-type="email"  data-parsley-trigger="keyup" />
					</div></i>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
						<span class="glyphicon glyphicon-earphone"></span>&nbsp; <label>Phone No</label> 
						<input type="text" name="phone" id="phone" class="form-control" placeholder="Phone No" style="width: 96%;" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup" /><br>
					</div>
				</div>
                </div>
				<div class="row">
						<div class="form-group">
							<div class="col-md-3 col-xs-5">
					&nbsp;&nbsp;&nbsp;&nbsp;<label>	<span class="glyphicon glyphicon-th-list"></span>&nbsp;Category </label> 
					 </div>
					 <div class="col-md-3 col-xs-6">
						  
						<select id="catg" name="catg"  class="form-control fa" style="margin-top:-8px; " required>

							<option  value="  ">Select Any One</option>
							<option style="height:60px;"   value="E-Commerce Solution"><i >&#xf07a;</i> &nbsp;E-Commerce Solution</option>
							<option style="height:60px;"  value="E-Learning Platform"><i >&#xf0c0;</i> &nbsp;E-Learning Platform</option>
							<option style="height:60px;"  value="Billing & Order Solution"><i >&#xf02f;</i> &nbsp;Billing & Order Solution</option>
							<option style="height:60px;"  value=" Health Care"><i >&#xf0f0;</i> &nbsp; Health Care</option>
							<option style="height:60px;"  value="Office Related Software"><i >&#xf07a; </i> &nbsp;Office Related Software</option>
							<option style="height:60px;"   value=" Media & Entertainment"><i >&#xf008; </i> &nbsp;Media & Entertainment</option>
							<option style="height:60px;" value=" Portfolio Management"><i >&#xf007; </i> &nbsp;Portfolio Management</option>
							<option style="height:60px;"   value="Mass Communication"><i >&#xf1c7; </i> &nbsp;Mass Communication</option>


						  </select>
					</div>
				</div>
                </div>
<br><br>



				<label for="subjects" >
					&nbsp;&nbsp;<i class="glyphicon glyphicon-check">&nbsp;</i>Your Requirement
					</label>
					<div style="overflow-x:auto;">

					<center><table class="table table-bordered  bg-info " style="width: 1040px;height:400px" >
						<thead >
		
<tr>
	<th><i class="glyphicon glyphicon-globe" >&nbsp;</i>Website</th>
<td>     <input style="height:16px;width:20px;" type="checkbox"  class="form-check-input" id="check01" name="vehicle01[]" value="Static" required>&nbsp;&nbsp;<font size="2" >Static</font>&nbsp; <span class="glyphicon glyphicon-leaf" ></span></input>   
									   
</td>
	<td>    <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check02" name="vehicle01[]" value="Dynamic">&nbsp;&nbsp;<font size="2">Dynamic</font> &nbsp;<i class="glyphicon glyphicon-send"></i></input>   
									   
</td>
<td>  <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check03" name="vehicle01[]" value="Super">&nbsp;&nbsp;<font size="2">Super Dynamic</font>&nbsp; <i style="font-size:15px" class="fa">&#xf135;</i></input>   
									   
</td>
</tr>
<tr>
         	<th><i class="glyphicon glyphicon-phone" >&nbsp;</i>Mobile App</th>
		<td>    <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check1" name="vehicle01[]" value="Android App" >&nbsp;&nbsp;</i><font size="2">Android App</font> &nbsp;<i class="fab fa-android"></i></input>   
											    
		 </td>
			 <td>     <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check2" name="vehicle01[]" value="IOS App" >&nbsp;&nbsp;<font size="2">IOS App</font> &nbsp;    <span class="glyphicon">&#xf8ff;</span></input>   
											    
		 </td>
		 <td>     <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check3" name="vehicle01[]" value="Hybrid App"  >&nbsp;&nbsp;<font size="2">Hybrid App</font> &nbsp;<span class="glyphicon glyphicon-th"></span></input>   
											    
	</td>
</tr>

<tr>
	<th><i style='font-size:15px' class='fas'required>&#xf653;&nbsp;&nbsp;</i>Digital Markrting</th>
<td>  <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check0" name="vehicle01[]" value="Content Writing">&nbsp;&nbsp;<font size="2">Content Writing</font>&nbsp; <i style="font-size:15px" class="fa">&#xf044;</i></input>   
									   
</td>
	<td>  <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check10" name="vehicle01[]" value="SEO">&nbsp;&nbsp;<font size="2">SEO</font> &nbsp;<i style="font-size:15px" class="fa">&#xf00e;</i></input>   
									   
</td>
<td>    <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="check00" name="vehicle01[]" value="Social Media">&nbsp;&nbsp;<font size="2">Social Media </font>&nbsp;<span class="glyphicon">&#xe010;</span></input>   
									   
</td>
</tr>
<tr>
<th> <i style='font-size:15px' class='fas'required>&#xf688;&nbsp;&nbsp;</i>	 Other</th>
<td> 	<input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="checkk " name="vehicle01[]" value="Hosting">&nbsp;&nbsp;<font size="2">Hosting</font> &nbsp;<i class="glyphicon glyphicon-cloud"></i></input>   
									   
</td>
	<td>  <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="checkk0" name="vehicle01[]" value="SSL">&nbsp;&nbsp;<font size="2">SSL</font> &nbsp;<i class="glyphicon glyphicon-lock"></i></input>   
									   
</td>
<td> <input style="height:16px;width:20px;" type="checkbox" class="form-check-input" id="checkk2" name="vehicle01[]" value="other"  > &nbsp;&nbsp;<font size="2">other </font>&nbsp;<span class="glyphicon glyphicon-cog"></input>   
									   
</td>
</tr>
</thead></table></center> </div>


&nbsp;<i style="font-size:15px" class="fa">&#xf044; </i><label> &nbsp;&nbsp;Exact Requirement</label>
<center><textarea style="height:150px;width:70%;" rows="5" cols="50" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="message" placeholder="Requirement"></textarea></center><br>
 

&nbsp; <label style="margin-top:-10px;">  <i style="font-size:15px" class="fa  ">&#xf249;</i>&nbsp;&nbsp;Note</label>
  <center><textarea style="height:90px;width:70%; margin-top:-30px;" rows="3" cols="30" class="form-control" id="note" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="note" placeholder="Note.."></textarea></center><br>
 

  <center><input style="height:40px;width:40%;"  type="submit" name="submit" id="submit" value="Submit" class="btn btn-success" /></center>
  </div>  </div>

  </form><br>













  <script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+91 8793363725", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
 

  <body>
 
      </html> 
 
                                    






  



