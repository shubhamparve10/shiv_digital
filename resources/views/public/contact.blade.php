@extends('layouts.app')

@section('css')
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
  
	<style>


	/* a:hover {
	    background-color: white;
	}
	h2 a:hover {
text-decoration: none;
} */
/* Small devices (landscape phones, 544px and up) */
@media (max-width: 544px) {
  h2 {font-size:1.5rem;}
	h4 {font-size:1.0rem;} /*1rem = 16px*/
}
@media (min-width: 544px) {
  h2 {font-size:2rem;}
	h4 {font-size:1.3rem;} /*1rem = 16px*/
}

/* Medium devices (tablets, 768px and up) The navbar toggle appears at this breakpoint */
@media (min-width: 768px) {
    h2 {font-size:2.5rem;}
		h4 {font-size:1.5rem;} /*1rem = 16px*/
}

/* Large devices (desktops, 992px and up) */
@media (min-width: 992px) {
    h2 {font-size:3rem;}
		h4 {font-size:1.7rem;}/*1rem = 16px*/
}

/* Extra large devices (large desktops, 1200px and up) */
@media (min-width: 1200px) {
    h2 {font-size:3.5rem;}
		h4 {font-size:2.0rem;} /*1rem = 16px*/
}
@-webkit-keyframes blinker {
  from {opacity: 1.0;}
  to {opacity: 0.0;}
}
.blink{
	text-decoration: blink;
	-webkit-animation-name: blinker;
	-webkit-animation-duration: 0.6s;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-timing-function:ease-in-out;
	-webkit-animation-direction: alternate;
}


#box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
}
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}

input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    color: #468847;
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    color: #B94A48;
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    margin: 2px 0 3px;
    padding: 0;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }

  .parsley-type, .parsley-required, .parsley-equalto, .parsley-pattern, .parsley-length{
   color:#ff0000;
  }

  a {
    font-family: " Helvetica Neue,Helvetica,sans-serif";
    color: black;
 }
</style>
@endsection

@section('js')
<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
<script src="{{ asset('js/articles/showarticle.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://parsleyjs.org/dist/parsley.js"></script>
<script>$(document).ready(function(){
    $('#contactForm').parsley();

    
});
</script>

@endsection

@section('content')
	<div class="container" style="margin-top: 2vh;">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="page-header">
					<h1 class="text-center" id='box'>Get in Touch With us</h1>
				</div>
			</div>
		</div>
		</div>
		 
	<div class="container">
    <div class="row">
      <center>
			<div class="col-lg-3 col-md-2 col-sm-2 col-xs-3">
				<h2 class="text-center"><a style="color:#3b5998; font-size:45px;" class="fa fa-facebook-square fa-1x" href=""></a></h2>
 			</div>
			<div class="col-lg-3 col-md-2 col-sm-2 col-xs-3">
				<h2 class="text-center"><a style="color:#e4405f;font-size:45px;" class="fa fa-instagram fa-1x" href=""></a></h2>
 			</div>
		<div class="col-lg-3 col-md-2 col-sm-2 col-xs-3">
				<h2 class="text-center"><a class="fa fa-envelope fa-1x" style="color:#B23121; font-size:45px;"  aria-hidden="true"href="mailto:?subject=&body=:%20" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' + encodeURIComponent(document.URL)); return false;"></a></h2>
 			</div>
	 
		<div class="col-lg-3 col-md-2 col-sm-2 col-xs-3">
				<h2 class="text-center"><a style="color:#25D366; font-size:45px;" class="fa fa-whatsapp fa-1x"  href="https://wa.me/8983691455"></a></h2>
       </div>
      
      
     
      <center/>
     
    </div> <hr>
		</div>
 	</div> 
 


			<div class="container" >
      <div class="row">
      <div class="col-md-12" >
 

<?php



function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}


$user_ip = getUserIP();

echo $user_ip; // Output IP address [Ex: 177.87.193.134]


?>
          <form id="validate_form" class="col-md-8" method="post" action="{{ url('/contact/insert') }}" name="sentMessage" novalidate>
            <h2 style="text-align:center"><b><u>Send us a Message</u></b></h2> <br><br>

            {{ csrf_field() }}

            <div class="control-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 groove" style="margin-left:-1em;">
              <div class="controls">

              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7"style="margin-left:-5px;">
                <label style="margin-top:5px;"><i class="fa fa-pencil-square"></i>&nbsp;Name</label>
              </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                <input type="text" class="form-control"style="margin-left:-10px; border-radius: 10px; border-color:gray" id="name" placeholder="Enter Name" required data-validation-required-message="Please enter your name." name="name" required data-parsley-pattern="[a-zA-Z]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup">
                <p class="help-block"></p>    </div>
              </div>
            </div>
      
            <div class="control-group form-group">
              <div class="control-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 groove"style="margin-left:-1em;">

              <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7"style="margin-left:-5px;">

                  <label style="margin-top:5px; border-radius: 10px; border-color:gray"><i class="fa fa-phone"></i>&nbsp;Number</label>
              </div>
              <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                <input type="tel" class="form-control" style="margin-left:-10px; border-radius: 10px; border-color:gray" id="phone" required data-validation-required-message="Please enter your phone number." name="phone" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup">
              </div>
            </div>
            </div></div>


            <div class="control-group form-group">
              <div class="control-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 groove"style="margin-left:-1em;">

              <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7"style="margin-left:-5px;">

                  <label style="margin-top:5px;"><i class="fa fa-envelope"></i>&nbsp;Email </label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                <input type="email" class="form-control" style="margin-left:-10px; border-radius: 10px; border-color:gray" id="email" required data-validation-required-message="Please enter your email address." name="email" placeholder="Email" required data-parsley-type="email"  data-parsley-trigger="keyup">
              </div>
            </div>
            </div>
          </div>

            <div class="control-group form-group"> 
              <div class="control-group form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 groove"style="margin-left:-1em;">

              <div class="controls">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7"style="margin-left:-5px;">

                <label style="margin-top:5px;"><i class="fa fa-commenting"></i>&nbsp;Message:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                <textarea rows="10" cols="100" style="margin-left:-10px; border-radius: 10px; border-color:gray"  class="form-control" id="message" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="message" placeholder="Message"></textarea><br>
              </div>
            </div>
              </div>
            </div>

 
 
            <center>  <br><button style="width:60%; margin-left:20px" type="submit" class="btn btn-primary" id="sendMessageButton">Send Message</button></center>
          </form> <br><br> <br> 
      
       
      
				<div class="col-md-4 "  >
          <h3><b>Contact Details</b></h3><hr>
          <h4>Contact  Number</h4>
          <p>
             <i class="fa fa-phone blink  " style="font-size:19px;color:blue;"></i>&nbsp;&nbsp;Contact&nbsp;&nbsp;<a  href="tel:+91-8983691455 " style="text-decoration:none;font-size:22px; margin-left:25px;"> +91-8983691455 </a><br>
            <i class="fa fa-whatsapp blink  " style="font-size:19px;color:#25D366;"></i>&nbsp;&nbsp;Whatsapp <a  href="tel:+91-8983691455 " style="text-decoration:none;font-size:22px; margin-left:15px;"> +91-8983691455 </a><br><hr>
            <i class="fa fa-envelope blink  " style="font-size:19px;color:#B23121"></i>&nbsp;&nbsp;  <a  href="mailto:shivanshcreation04@gmail.com" style="text-decoration:none;font-size:22px; margin-left:15px;"> shivanshcreation04@gmail.com</a><br>
            {{-- <i class="fa fa-envelope blink  " style="font-size:19px;color:#B23121"></i>&nbsp;&nbsp;  <a  href="mailto:ibdcbaramati@gmail.com" style="text-decoration:none;font-size:22px; margin-left:15px;"> ibdcbaramati@gmail.com</a> --}}
     
          </p>
        
          

        
              </div>  </div>
              {{-- <map> --}}
              {{-- <div class="container col-md-4" >
    <iframe src="" height="280px" width="100%;" frameborder="2" style="border:2; margin-bottom:0px;" allowfullscreen></iframe>
      </div> --}}
       
</div>


</div>

</div>

<br> <br> 
@endsection

