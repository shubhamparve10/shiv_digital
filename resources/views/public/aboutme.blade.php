@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/justifiedGallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lg-transitions.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/articles/showarticle.css') }}" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<style>
	input[type=text]:focus {
	border-color:black ;
	}
	.img-responsive, .thumbnail>img, .thumbnail a>img, .carousel-inner>.item>img, .carousel-inner>.item>a>img {
    display: block;
  width: 700px;

	height: 400px;
	max-height: 100%;
}
blockquote {
		padding: 11px 22px;
		margin: 0 0 22px;
		font-size: 20px;
		border-left: 5px solid black;
}
.lead {
    margin-bottom: 22px;
    font-size: 24px;
    font-weight: 300;
    line-height: 1.4;
}
	</style>
@endsection
@section('js')
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
	<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('js/articles/showarticle.js') }}"></script>
@endsection
@section('content')
	<div class="container-fluid">
			@if (!is_null($results))
			<div class="row" style="margin:auto;width:80%;">
				<div class="col-md-12">
					<div class="list-group">
						@if (!$results->isEmpty())
							@foreach($results as $result)
								<!-- <a href="{{ url('/aboutme/get/'.$result->id) }}" class="list-group-item"> -->
									<div class="row">
											<img class="img-responsive img-thumbnail" src="{{ asset($result->path) }}"/>
												<br>
													<br>
														<br>
												 <div class="col-xs-10 col-xs-offset-1">
														<blockquote>
															<h2>QuirkyPurvi</h2>
														</blockquote>
													</div>
													<br>
													<br><br><br><br>
													<p class="lead" style="padding: 25px 35px 0px 15px;float:center;">{{ $result->caption }}
														</p><br>
													<p class="lead" style="padding: 25px 35px 0px 15px;float:center;">{{ $result->caption1 }}
														</p><br>
													<p class="lead" style="padding: 25px 35px 0px 15px;float:center;">{{ $result->caption2 }}
														</p>
										</div>
									</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
		@endif
		@if (!is_null($results))
			@if (!$results->isEmpty())
				<div class="row">
					<div class="col-md-6 col-md-offset-5">
						{{ $results->links() }}
					</div>
				</div>
			@endif
		@endif
	</div>

@endsection
