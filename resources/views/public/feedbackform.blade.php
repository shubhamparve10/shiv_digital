@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/css/fileinput.min.css">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-tagsinput.css') }}" />
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<style>
	input[type=text]:focus{
	    border-color: black;
	}
	input[type=file]:focus {
	    border-color: black;
	}
		textarea[class=form-control]:focus {
	border-color: black;
	}
	select[class=form-control]:focus {
	border-color: black;
	}
	input[type=text]:hover {
			border-color: black;
	}
	div.stars {
		float:left;
	  width: 300px;
	  display: inline-block;
	}

	input.star { display: none; }

	label.star {
	  float: right;
	  padding: 10px;
	  font-size: 36px;
	  color: #444;
	  transition: all .2s;
	}

	input.star:checked ~ label.star:before {
	  content: '\f005';
	  color: #FD4;
	  transition: all .25s;
	}

	input.star-5:checked ~ label.star:before {
	  color: #FE7;
	  text-shadow: 0 0 20px #952;
	}

	input.star-1:checked ~ label.star:before { color: #F62; }

	label.star:hover { transform: rotate(-15deg) scale(1.3); }

	label.star:before {
	  content: '\f006';
	  font-family: FontAwesome;
	}
	.btn {
  background-color: white ;
  border: 1px solid black;;
  color: black;
  padding: 16px 32px;
  text-align: center;
  font-size: 16px;
  margin: 4px 2px;
  transition: 0.3s;
}

.btn:hover {
  background-color: #ddd;
  color: black;
}

	</style>
@endsection

@section('js')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
@endsection

@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<div class="container" style="margin-top: 10vh;">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="page-header">
					<h1 class="text-center"> Give Us Feedback  </h1>
				</div>

			</div>
		</div>
	</div>
	<div class="container">

		<center><form class="row" method="post"  action="{{ url('/feedbacks/insert') }}"enctype="multipart/form-data">
			{{ csrf_field() }}
			
			<div class="control-group form-group">

<div class="controls">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label style="margin-top:10px;margin-right:70px;"><i class="glyphicon glyphicon-pencil"style="margin-right:8px"></i>  Name:</label>
  </div>
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
  <input type="text" class="form-control" id="name" placeholder="Enter Name" required data-validation-required-message="Please enter your name." name="name" required data-parsley-pattern="[a-zA-Z ]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup">
  <p class="help-block"></p> 
  </div>
</div>
</div>

<div class="control-group form-group">
<div class="controls">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
  <label style="margin-top:10px;"><i class="glyphicon glyphicon-phone"style="margin-right:10px"> </i>Phone Number:</label>
  </div>
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
  <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number." name="cnumber" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup">
  <p class="help-block"></p>  </div>
  <!-- <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number." name="cnumber" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup"> -->
</div>
</div> 
<div class="control-group form-group">
<div class="controls">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
  <label style="margin-top:10px;"> <i class="glyphicon glyphicon-envelope"style="margin-right:10px"> </i>Email Address:</label>
  </div>
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
  <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address." name="email" placeholder="Email" required data-parsley-type="email"  data-parsley-trigger="keyup">
  <p class="help-block"></p></div>
</div>
</div>
				<br><br>
				<div class="form-row">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<label for="title" style="margin-top:25px;margin-right:54px;"><i class="fa fa-star" aria-hidden="true"></i>
Rating:</label>
						</div>	
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="stars">
								<input class="star star-5" value="5"  id="star-5" type="radio" name="star"/>
								<label class="star star-5" for="star-5"></label>
								
								<input class="star star-4" value="4"  id="star-4" type="radio" name="star"/>
								<label class="star star-4" for="star-4"></label>
								<input class="star star-3" value="3"  id="star-3" type="radio" name="star"/>
								<label class="star star-3" for="star-3"></label>
								<input class="star star-2" value="2"  id="star-2" type="radio" name="star"/>
								<label class="star star-2"  for="star-2"></label>
								<input class="star star-1" value="1"  id="star-1" type="radio" name="star"/>
								<label class="star star-1" for="star-1"></label>
							</div>
						</div>			
					</div>					
				</div>
				
				<div class="form-row">
					<div class="col-lg-3 col-md-2 col-sm-12 col-xs-12">
						<label for="title" style="margin-top:15px;margin-right:54px;"><i class="fa fa-comments" aria-hidden="true"></i> Feedback:</label>
					</div>	
					<div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
						<textarea id="caption" placeholder="How was your experience?"  wrap="hard" rows="5" cols="10" name="caption" class="form-control" style="width:80%;margin-right:165px;" required>
							@if(isset($feedback))
							{{ $feedback->caption }}
							@endif
						</textarea><br>
					</div>
				</div>
			
			<br><br>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<button type="submit" class="btn btn-primary btn-raised col-lg-7 col-md-7 col-sm-12 col-xs-12"  >Submit</button>
				</div>	
		</form></center>
	</div><br><br><br>
@endsection
