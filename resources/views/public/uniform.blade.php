
    @extends('layouts.app')

@section('css')
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
  
	<style>
        #box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
}


	/* a:hover {
	    background-color: white;
	}
	h2 a:hover {
text-decoration: none;
} */
/* Small devices (landscape phones, 544px and up) */
@media (max-width: 544px) {
  h2 {font-size:1.5rem;}
	h4 {font-size:1.0rem;} /*1rem = 16px*/
}
@media (min-width: 544px) {
  h2 {font-size:2rem;}
	h4 {font-size:1.3rem;} /*1rem = 16px*/
}

/* Medium devices (tablets, 768px and up) The navbar toggle appears at this breakpoint */
@media (min-width: 768px) {
    h2 {font-size:2.5rem;}
		h4 {font-size:1.5rem;} /*1rem = 16px*/
}

/* Large devices (desktops, 992px and up) */
@media (min-width: 992px) {
    h2 {font-size:3rem;}
		h4 {font-size:1.7rem;}/*1rem = 16px*/
}

/* Extra large devices (large desktops, 1200px and up) */
@media (min-width: 1200px) {
    h2 {font-size:3.5rem;}
		h4 {font-size:2.0rem;} /*1rem = 16px*/
}
@-webkit-keyframes blinker {
  from {opacity: 1.0;}
  to {opacity: 0.0;}
}
.blink{
	text-decoration: blink;
	-webkit-animation-name: blinker;
	-webkit-animation-duration: 0.6s;
	-webkit-animation-iteration-count:infinite;
	-webkit-animation-timing-function:ease-in-out;
	-webkit-animation-direction: alternate;
}


#box {
  margin: 30px auto 0 auto;
  animation: appear 4s  ;
 }
@keyframes appear {
  0% {
    transform: translate3d(-500px, 0px, 0px);
    opacity: 0;
  }
  50%{
    opacity: 0.2;
    transform: translate3d(-350px, 0px, 0px);
  }
  100%{
    opacity: 1;
    transform: translate3d(0px, 0px, 0px);
  }
}

input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    color: #468847;
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    color: #B94A48;
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    margin: 2px 0 3px;
    padding: 0;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }

  .parsley-type, .parsley-required, .parsley-equalto, .parsley-pattern, .parsley-length{
   color:#ff0000;
  }
</style>
@endsection

@section('js')
<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
<script src="{{ asset('js/articles/showarticle.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="http://parsleyjs.org/dist/parsley.js"></script>
<script>
$(document).ready(function(){
    $('#validate_form').parsley();

    
});
</script>
</script>

@endsection

@section('content') 
<div class="container" style="margin-top: 0em;">
		<div class="row">
			<div class="col-xs-12 col-xs-offset-0" >
			<center>	<h2 id='box'>
                    Form
				    </h2></center>
		 
			</div>
		</div>
    </div>

	<div class="container" style="margin-top:  1em;">
		<div class="row">
			<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="page-header">
					<h1 class="text-center" id='box'>Get in Touch With us</h1>
				</div>
			</div> -->
		</div>
        </div>
        
        <!-- <center><h2 id="box" style="margin-right:8em;margin-bottom:0em">   
                    Uniform
				    </h2></center> -->
		 


			<div class="container" >
      <div class="row">
      <div class="col-lg-12 col-md-7 col-sm-12 col-xs-12 groove " style="">
          <!-- <h3 style="text-align:center"><b style="font-family:Rockwell;color:black;" >Form</b></h3> -->
       
          <?php



function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}


$user_ip = getUserIP();

echo $user_ip; // Output IP address [Ex: 177.87.193.134]


?>
          <!-- <form name="sentMessage" id="contactForm" style=margin-left:2em; novalidate > -->
          <form id="validate_form"  method="post" action="{{ url('/uniform/insert') }}" name="sentMessage" >

						{{ csrf_field() }}
            <div class="col-md-12 col-xs-12" style="font-family:ubuntu; text-color:#777; border: 2px solid black ;border-radius: 8px;">
            <br><br>
            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <label><i class="glyphicon glyphicon-pencil"style="margin-right:9px; margin-top:10px;"></i>Full Name:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
                <input type="text" class="form-control" id="name" placeholder="Enter Name" required data-validation-required-message="Please enter your name." name="name" required data-parsley-pattern="[a-zA-Z ]+$" data-parsley-pattern-message="This field only contains alphabets" data-parsley-trigger="keyup">
                <p class="help-block"></p> 
                </div>
              </div>
            </div>

            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                <label><i class="glyphicon glyphicon-phone"style="margin-right:9px; margin-top:10px;"></i>Phone Number:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">	
                <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number." name="phone" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup">
                <p class="help-block"></p>  </div>
                <!-- <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number." name="cnumber" placeholder="Phone No" required data-parsley-pattern="[0-9]+$" data-parsley-length="[10,12]" data-parsley-pattern-message="Phone No should be in digits only" data-parsley-length-message="Phone No should be of (10-12) digits" data-parsley-trigger="keyup"> -->
              </div>
            </div> 
            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">   
                <label><i class="glyphicon glyphicon-envelope"style="margin-right:9px; margin-top:10px;margin-top:10px;"></i>Email Address:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address." name="email" placeholder="Email" required data-parsley-type="email"  data-parsley-trigger="keyup">
                <p class="help-block"></p><br></div>
              </div>
            </div>

            <div class="control-group form-group">
            <div class="controls" >
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">  
            <label><i class="fa fa-user-circle-o" style="margin-right:4px; " aria-hidden="true" ></i> Uniform For:</label>
            </div>
			<div class=" form-radio"  >
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
  <input type="radio" name="school" value="School" required data-validation-required-message="Please enter your email address." > School &nbsp;
  <input type="radio" name="school" value="College"> College &nbsp;
  <input type="radio" name="school" value="Company"> Company <br> &nbsp;
  <p class="help-block">
  </div>  </div><br><br>
  </div> 
         

            <div class="control-group form-group">
              <div class="controls">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
                <label><i class="fa fa-comments" style="margin-right:4px" aria-hidden="true"></i> Description:</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxsength="999" style="resize:none" name="message" placeholder="Message"></textarea>
                <p class="help-block"></div></div></div>
              
            </div>
            <div id="success"></div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <p style="line-height:28px">
					</div>
          <div class="form-row">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					</div>
					<div class="col-sm-12 col-md-10 col-xs-12 col-lg-10"  ><p style="line-height:30px">
					<br>	<button type="submit"  class="btn btn-primary  col-md-4  " style="margin-left:90px"   id="sendMessageButton">submit</button><br>
						</p><br><br>
					</div>
				</div>
              
            <!-- <center>  <button  class="btn btn-primary  col-md-5  " style="font-size:px" >submit</button></center> -->
            </div><br>
            </div>
 
              </div>


             </div>
          <br><br><br>
</div>
@endsection


