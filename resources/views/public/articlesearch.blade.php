@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/justifiedGallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lightgallery.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/lg-transitions.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/articles/showarticle.css') }}" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
	<link rel="shortcut icon" href="images/NeetaTechLogo.jpg" />

	<style>
	input[type=text]:focus {
	border-color:black ;
	}
	.img-responsive, .thumbnail>img, .thumbnail a>img, .carousel-inner>.item>img, .carousel-inner>.item>a>img {
    display: block;
  	width: 250px;
		height: 170px;

}
	</style>
@endsection
@section('js')
	<script src="{{ asset('js/jquery.scrollex.min.js') }}"></script>
	<script src="{{ asset('js/jquery.justifiedGallery.min.js') }}"></script>
	<script src="{{ asset('js/lightgallery-all.min.js') }}"></script>
	<script src="{{ asset('js/articles/showarticle.js') }}"></script>
@endsection
@section('content')>
	<div class="container-fluid">
		<div class="row" >
			<form class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;" method="POST" action="{{ url('/blogs/search/') }}" role="search">
					<div class="form-group" >
							<form class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="input-group" style="width:60%;margin:auto;padding-top:5px;" >
							<input type="text" class="form-control" name="searchNavbar" style="position: relative;" placeholder="Filter by tag" >
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary btn-navbar"style="background-color:black;border-color:black;"><span class="glyphicon glyphicon-search"></span></button>
							</span>
						</div>
					</div>
				</form>
		</div>
		@if (!is_null($results))
			<div class="row" style="margin:auto;width:80%;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="list-group">
						@if (!$results->isEmpty())
							@foreach($results as $result)
								<a href="{{ url('/articles/show/'.$result->id) }}" class="list-group-item">
									<div class="row">
										<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
											<img class="img-responsive img-thumbnail" src="{{ asset($result->path) }}"/>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" >
											<!-- <div class="least-content pull-right" style="font-size:17px;">{{ $result->created_at->timezone('Asia/Kolkata')->format('d/m/Y') }}</div> -->
											<h4 class="list-group-item-heading" style="font-size:25px; padding: 25px 0px 0px 40px;">{{ $result->title }}</h4>
											<p class="list-group-item-heading"style="font-size:18px; padding: 25px 35px 0px 15px;float:center;">{{ $result->shortdescription }}<br><h4 class="pull-right">Read More...</h4>
											</p>
										</div>
									</div>
								</a><hr>
							@endforeach
						@else
							<h3 style="text-align:center;">No Such Services Exists. Please use another search term</h3>
						@endif
					</div>
				</div>
			</div>
		@endif
		@if (!is_null($results))
			@if (!$results->isEmpty())
				<div class="row">
					<div class="col-md-6 col-md-offset-5">
						{{ $results->links() }}
					</div>
				</div>
			@endif
		@endif
	</div>

@endsection
