@extends('layouts.app')
 
@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<br><br>
	<div style="float:left;padding-left:250px;padding-top:10px;">
<a style="background-color:white;color:black;border-color:black;"class="btn btn-primary btn-lg " href="{{ url('admin/') }}" >Back</a>
</div>
	<div class="container" style="padding-top:30px;">
		<div class="page-header row">
			<h3 class="col-md-8 col-sm-8 col-xs-8" >Manage Images </h3>
			<a style="background-color:white;color:black;border-color:black;"class="btn btn-primary btn-lg col-md-4 col-sm-4 col-xs-4" href="{{ url('admin/images/create') }}">New Image</a>
		</div>
	</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Image Id</th>
							<th>Image Title</th>
							<th>Image Caption</th>
							<th>Image Author</th>
							<th>Image Date</th>
							<th>Image Rating</th>
							<th>Image Actions</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($images))
							@foreach($images as $image)
								<tr>
									<td>{{ $image->id }}</td>
									<td>{{ $image->title }}</td>
									<td>{{ $image->caption }}</td>
									<td>{{ $image->user->username }}</td>
									<td>{{ $image->created_at->timezone('Asia/Kolkata')->format('d/m/Y h-i-s A') }}</td>
									<td>{{ $image->rating }}</td>
									<td>
										<div class="btn-group" role="group">
											<a style="background-color:white;color:black;border-color:black;" class="btn btn-primary" href="{{ url('/admin/images/edit/'.$image->id) }}">Edit</a>
											<a class="btn btn-danger" href="{{ url('/admin/images/delete/'.$image->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div><br> 
	
@endsection
