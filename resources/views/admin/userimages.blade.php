@extends('layouts.app')

@section('content')
<br><br><br><br><br><br>
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<div style="float:left;padding-left:250px;padding-top:10px;">
	<a style="background-color:white;color:black;border-color:black;"class="btn btn-primary btn-lg " href="{{ url('admin/') }}" >Back</a>
	</div>
	<div class="container" style="padding-top:30px;">
			<div class="page-header row">
			<h3 class="col-md-8 col-sm-8 col-xs-8">Manage Blogs </h3>
			<a style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg col-md-4 col-sm-4 col-xs-4" href="{{ url('admin/product/create') }}">New Blog</a>
		</div>
	</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Blog Id</th>
							<th>Blog Title</th>
							<th>Blog Date</th>
							<th>Blog Actions</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($images))
							@foreach($images as $image)
								<tr>
									<td>{{ $image->id }}</td>
									<td>{{ $image->title }}</td>
									<td>{{ $image->created_at->timezone('Asia/Kolkata')->format('d/m/Y h-i-s A') }}</td>
									<td>
										<div class="btn-group" role="group">
											<a class="btn btn-primary" style="background-color:white;color:black;border-color:black;" href="{{ url('/admin/userimages/edit/'.$image->id) }}">Edit</a>
											<a class="btn btn-danger" href="{{ url('/admin/userimages/delete/'.$image->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
