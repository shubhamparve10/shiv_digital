@extends('layouts.app')
<br>
<br><br>
@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
<div class="container" style="padding-top:30px;">
				<div class="page-header row">
	<a class="btn btn-primary btn-lg " style="font-size:20px;background-color:white;color:black;border-color:black;" href="{{ url('admin/') }}" >Back</a>

					<a  class="btn btn-primary btn-lg col-md-4 col-sm-4 col-xs-4 pull-right" href="{{ url('admin/userblogs/create') }}"style="font-size:20px;background-color:white;color:black;border-color:black;">New Package</a>
						<h3 class="col-md-8 col-sm-8 col-xs-8">Manage Blogs </h3><br>
		</div>
	</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Package Id</th>
							<th>Package Title</th>
							<th>Package Date</th>
							<th>Package Actions</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($blogs))
							@foreach($blogs as $blog)
								<tr>
									<td>{{ $blog->id }}</td>
									<td>{{ $blog->title }}</td>
									<td>{{ $blog->created_at->timezone('Asia/Kolkata')->format('d/m/Y h-i-s A') }}</td>
									<td>
										<div class="btn-group" role="group">
											<a class="btn btn-primary"  href="{{ url('/admin/userblogs/edit/'.$blog->id) }}"style="background-color:white;color:black;border-color:black;">Edit</a>
											<a class="btn btn-danger" href="{{ url('/admin/userblogs/delete/'.$blog->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
		</div>
 <BR>
@endsection
