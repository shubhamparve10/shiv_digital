@extends('layouts.app')

@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<
<div class="container" style="padding-top:30px;">
				<div class="page-header row">
	<a class="btn btn-primary btn-lg " style="font-size:20px;background-color:white;color:black;border-color:black;" href="{{ url('admin/') }}" >Back</a>
</div>
	</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
				<table class="table table-hover col-md-12" >
					<thead>
						<tr>
							<th class="col-md-2">Id</th>
							<th class="col-md-2">Name</th>
							<th class="col-md-2">Email</th>
							<th class="col-md-2">Number</th>
							<th class="col-md-4">Message</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($contacts))
							@foreach($contacts as $contact)
								<tr>
									<td >{{ $contact->id }}</td>
									<td >{{ $contact->name }}</td>
									<td >{{ $contact->email }}</td>
									<td >{{ $contact->cnumber }}</td>
									<td width=400%>{{ $contact->message }}</td>

									<td>
										<div class="btn-group" role="group">
											<a class="btn btn-danger" href="{{ url('/admin/contact/delete/'.$contact->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
		</div>
	</div>
@endsection
