@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/css/fileinput.min.css">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-tagsinput.css') }}" />
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
	<style>
	input[type=text]:focus{
			border-color: black;
	}
	input[type=file]:focus {
			border-color: black;
	}
		textarea[class=form-control]:focus {
	border-color: black;
	}
	select[class=form-control]:focus {
	border-color: black;
	}

	textarea[class=summernote]:focus {
border-color: black;
}
	</style>
@endsection



@section('js')
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/articlesform.js') }}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
@endsection


@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<br><br> 

	<div class="container">
		<form class="row" method="post" action="
		@if(isset($blog))
			{{ url('/admin/userblogs/update/'.$blog->id) }}
		@else
			{{ url('/admin/userblogs/insert') }}
		@endif "
		enctype="multipart/form-data">
			{{ csrf_field() }}

			<div class="col-md-12">
				<div class="form-group">
					<label for="title">Package Title</label>
					<input type="text" placeholder="Enter Package title.." class="form-control" id="title" name="title" value="{{ $blog->title or '' }}"required/>
				</div>
				<div class="form-group">
					<label for="imagefile">Package Image
						@if(isset($image)) {{ 'Leave Blank to not change image' }} @endif
					</label>
					<input type="file" placeholder="Browse image.." class="form-control file" id="imagefile" name="imagefile"required/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="tagsselect">Package Tags</label>
					<select multiple="multiple" class="form-control" id="tagsselect" name="tagsselect[]" data-role="tagsinput"required>
						@if(isset($blog))
							@foreach($blog->tags as $tag)
								<option value="{{ $tag->tag }}">{{ $tag->tag }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
				<div class="col-md-12">
			<div class="form-group">
				<label for="summernote">Package Short Description</label>
				<textarea id="caption" placeholder="Short Describe Package.." name="caption"class="form-control" required>
					@if(isset($blog))
						{{ $blog->shortdescription }}
					@endif
				</textarea>
			</div>
						</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="summernote">Package Content</label>
					<textarea id="#summernote" placeholder="Enter Package content.." name="summernote" class="summernote" required>
						@if(isset($blog))
							{{ $blog->caption }}
						@endif
					</textarea>
				</div>
			</div>

			<div class="col-md-12">
				<button type="submit" class="btn btn-primary btn-raised col-md-12 col-xs-12" style="background-color:white;color:black;border-color:black;">Upload Package</button>
			</div>
		</form>
	</div><br><br><br>
@endsection
