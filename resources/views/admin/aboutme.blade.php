@extends('layouts.app')

@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
<div class="container" style="padding-top:30px;">
				<div class="page-header row">
	<a class="btn btn-primary btn-lg " style="font-size:20px;background-color:white;color:black;border-color:black;" href="{{ url('admin/') }}" >Back</a>

					<a  class="btn btn-primary btn-lg col-md-4 col-sm-4 col-xs-4 pull-right" href="{{ url('admin/aboutme/create') }}"style="font-size:20px;background-color:white;color:black;border-color:black;">New aboutme</a>
						<h3 class="col-md-8 col-sm-8 col-xs-8">Manage aboutmes </h3>
		</div>
	</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>aboutme Id</th>
							<th>aboutme Caption</th>
							<th>aboutme Date</th>
							<th>aboutme Actions</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($aboutmes))
							@foreach($aboutmes as $aboutme)
								<tr>
									<td>{{ $aboutme->id }}</td>
									<td>{{ $aboutme->caption }}</td>
									<td>{{ $aboutme->created_at->timezone('Asia/Kolkata')->format('d/m/Y h-i-s A') }}</td>
									<td>
										<div class="btn-group" role="group">
											<a class="btn btn-primary"  href="{{ url('/admin/aboutme/edit/'.$aboutme->id) }}"style="background-color:white;color:black;border-color:black;">Edit</a>
											<a class="btn btn-danger" href="{{ url('/admin/aboutme/delete/'.$aboutme->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
