@extends('layouts.app')<br><br>

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/css/fileinput.min.css">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-tagsinput.css') }}" />
@endsection

@section('js')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
@endsection

@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif

	<div class="container">
		<form class="row" method="post" action="
		@if(isset($video))
			{{ url('/admin/videos/update/'.$video->id) }}
		@else
			{{ url('/admin/videos/insert') }}
		@endif ">
			{{ csrf_field() }}
			<div class="col-md-12">
				<div class="form-group">
					<label for="title">Video Title</label>
					<input type="text" class="form-control" id="title" name="title" value="{{ $video->title or '' }}"/>
				</div>
				<div class="form-group">
					<label for="video">Youtube Video URL </label>
					<input type="text" class="form-control" id="video" name="video"  value= "{{ $video->path or ''}}"/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="tagsselect">Video Tags</label>
					<select multiple="multiple" class="form-control" id="tagsselect" name="tagsselect[]" data-role="tagsinput">
						@if(isset($video))
							@foreach($video->tags as $tag)
								<option value="{{ $tag->tag }}">{{ $tag->tag }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="summernote">Video Caption</label>
					<textarea id="caption" name="caption" class="form-control">
						@if(isset($video))
							{{ $video->caption }}
						@endif
					</textarea>
				</div>
			</div>

			<div class="col-md-12">
				<button style="background-color:white;color:black;border-color:black;" type="submit" class="btn btn-primary btn-raised col-md-12 col-xs-12">Upload Video</button>
			</div>
		</form>
	</div><br><br><br><br>
@endsection
