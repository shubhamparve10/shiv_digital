@extends('layouts.app')<br> 

@section('content')
	@if(session('success')) 
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<div style="float:left;padding-left:250px;padding-top:10px;">
	<a style="background-color:white;color:black;border-color:black;"class="btn btn-primary btn-lg " href="{{ url('admin/') }}" >Back</a>
	</div>
	<div class="container" style="padding-top:30px;">
		<div class="page-header row">
			<h3 class="col-md-7 col-sm-7 col-xs-7">Manage Videos </h3>
			<a style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg pull-right col-md-4 col-sm-4 col-xs-4" href="{{ url('admin/videos/create') }}">New Video</a>
		</div>
	</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Video Id</th>
							<th>Video Title</th>
							<th>Video Caption</th>
							<th>Video Author</th>
							<th>Video Date</th>
							<th>Video Rating</th>
							<th>Video Actions</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($videos))
							@foreach($videos as $video)
								<tr>
									<td>{{ $video->id }}</td>
									<td>{{ $video->title }}</td>
									<td>{{ $video->caption }}</td>
									<td>{{ $video->user->username }}</td>
									<td>{{ $video->created_at->timezone('Asia/Kolkata')->format('d/m/Y h-i-s A') }}</td>
									<td>{{ $video->rating }}</td>
									<td>
										<div class="btn-group" role="group">
											<a style="background-color:white;color:black;border-color:black;"class="btn btn-primary" href="{{ url('/admin/videos/edit/'.$video->id) }}">Edit</a>
											<a class="btn btn-danger" href="{{ url('/admin/videos/delete/'.$video->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
		<br><br>
		<br><br>
	</div>
@endsection
