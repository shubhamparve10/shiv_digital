@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/css/fileinput.min.css">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-tagsinput.css') }}" />
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
@endsection

@section('js')
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/admin/articlesform.js') }}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
@endsection


@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif

	<div class="container"><br><br><br><br><br><br>
		<form class="row" method="post" action="
		@if(isset($image))
			{{ url('/admin/product/update/'.$image->id) }}
		@else
			{{ url('/admin/product/insert') }}
		@endif "
		enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">
				<div class="form-group">
					<label for="title">Blog Title</label>
					<input type="text" class="form-control" id="title" name="title" value="{{ $image->title or '' }}"/>
				</div>
				<div class="form-group">
					<label for="imagefile">Blog Image
						@if(isset($image)) {{ 'Leave Blank to not change image' }} @endif
					</label>
					<input type="file" class="form-control file" id="imagefile" name="imagefile"/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="tagsselect">Blog Tags</label>
					<select multiple="multiple" class="form-control" id="tagsselect" name="tagsselect[]" data-role="tagsinput">
						@if(isset($image))
							@foreach($image->tags as $tag)
								<option value="{{ $tag->tag }}">{{ $tag->tag }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-12">
		<div class="form-group">
			<label for="summernote">Blog Short Description</label>
			<textarea id="caption" name="caption" class="form-control" value="{{ $image->shortdescription or '' }}">
				@if(isset($image))
					{{ $image->shortdescription }}
				@endif
			</textarea>
		</div>
					</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="summernote">Blog Content</label>
					<textarea id="#summernote" name="summernote" class="summernote" >
						@if(isset($image))
							{{ $image->caption }}
						@endif
					</textarea>
				</div>
			</div>

			<div class="col-md-12">
				<button style="background-color:white;color:black;border-color:black;margin-bottom:5px;" type="submit" class="btn btn-primary btn-raised col-md-12 col-xs-12">Upload Blog</button>
			</div>
		</form>
	</div>
@endsection
