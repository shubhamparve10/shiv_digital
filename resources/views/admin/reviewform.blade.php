@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/css/fileinput.min.css">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-tagsinput.css') }}" />
@endsection

@section('js')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
@endsection

@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif

	<div class="container">
		<form class="row" method="post" action="
		@if(isset($aboutme))
			{{ url('/admin/aboutme/update/'.$aboutme->id) }}
		@else
			{{ url('/admin/aboutme/insert') }}
		@endif "
		enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">

				<div class="form-group">
					<label for="title">Name</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ $review->name }}"/>
				</div>
				<div class="form-group">
					<label for="title">Contact Number</label>
					<input type="number" class="form-control" id="number" name="number" value="{{ $review->number }}"/>
				</div>
				<div class="form-group">
					<label for="title">Destination</label>
					<input type="text" class="form-control" id="dest" name="dest" value="{{ $review->dest }}"/>
				</div>
				<div class="form-group">
					<label for="title">Type</label>
						<select class="selectpicker form-control"  id="type" name="type" value="{{ $review->number }}">
  						<option title="wedding">Wedding</option>
							<option title="travel">Trvel</option>
							<option title="wildlife">Wildlife</option>
							<option title="fashion">Fashion</option>
							<option title="blogs">Blogs</option>
						</select>
				</div>

				<div class="form-group">
					<label for="title">Anniversary Date</label>
					<input type="date" class="form-control" id="adate" name="adate" value="{{ $review->adate }}"/>
				</div>
				<div class="form-group">
					<label for="title">Birth Date</label>
					<input type="date" class="form-control" id="adate" name="adate" value="{{ $review->adate }}"/>
				</div>

			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="summernote">Aboutme</label>
					<textarea id="caption" name="caption" class="form-control">
						@if(isset($aboutme))
							{{ $aboutme->caption }}
						@endif
					</textarea>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="summernote">Aboutme1</label>
					<textarea id="caption1" name="caption1" class="form-control">
						@if(isset($aboutme))
							{{ $aboutme->caption1 }}
						@endif
					</textarea>
				</div>
			</div>
			<div class="col-md-12">
				<button type="submit"style="background-color:white;color:black;border-color:black;"  class="btn btn-primary btn-raised col-md-12 col-xs-12">Upload Aboutme</button>
			</div>
		</form>
	</div>
@endsection
