@extends('layouts.app')

@section('content')<br> 
	<div class="container">
		<div class="row" style="margin-top: 25px; margin-bottom:25px;">
			<div class="col-xs-12">
				<a type="button" style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg btn-block" href="{{ url('admin/images') }}">Image Admin</a>
			</div>
		</div>

		<div class="row" style="margin-top: 25px; margin-bottom:25px;">
			<div class="col-xs-12">
				<a type="button" style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg btn-block" href="{{ url('admin/videos') }}">Video Admin</a>
			</div>
		</div>

		<div class="row" style="margin-top: 25px; margin-bottom:25px;">
			<div class="col-xs-12" >
				<a type="button" style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg btn-block" href="{{ url('admin/userblogs') }}">New Arrival</a>
			</div>
		</div>

		<!-- <div class="row" style="margin-top: 25px; margin-bottom:25px;">
			<div class="col-xs-12" >
				<a type="button" style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg btn-block" href="{{ url('admin/aboutme') }}">About Me Admin</a>
			</div>
		</div> -->
		<!-- <div class="row" style="margin-top: 25px; margin-bottom:25px;">
			<div class="col-xs-12" >
				<a type="button" style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg btn-block" href="{{ url('admin/contact') }}">Contact Admin</a>
			</div>
		</div> -->
		<div class="row" style="margin-top: 25px; margin-bottom:25px;">
					<div class="col-xs-12" >
						<a type="button" style="background-color:white;color:black;border-color:black;" class="btn btn-primary btn-lg btn-block" href="{{ url('admin/feedbacks') }}">Feedback Admin</a>
					</div>
				</div>
				<br>
				<br>
				<br><br>
	</div>
@endsection
