@extends('layouts.app')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/css/fileinput.min.css">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-tagsinput.css') }}" />
@endsection

@section('js')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.2/js/fileinput.min.js"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
@endsection

@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif

	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif
	<br><br> 

	<div class="container">
		<form class="row" method="post" action="
		@if(isset($image))
			{{ url('/admin/images/update/'.$image->id) }}
		@else
			{{ url('/admin/images/insert') }}
		@endif "
		enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-md-12">
				<div class="form-group">
					<label for="title">Image Title</label>
					<input type="text" class="form-control" id="title" name="title" value="{{ $image->title or '' }}"/>
				</div>
				<div class="form-group">
					<label for="imagefile" >Image
						@if(isset($image)) {{ 'Leave Blank to not change image' }} @endif
					</label>
					<input type="file"  class="form-control file" id="imagefile" name="imagefile" />
				</div>
			</div>
			<!-- <div class="col-md-12">
				<div class="form-group">
					<label for="tagsselect">Image Tags</label>
					<select multiple="multiple" class="form-control" id="tagsselect" name="tagsselect[]" data-role="tagsinput">
						@if(isset($image))
							@foreach($image->tags as $tag)
								<option value="{{ $tag->tag }}">{{ $tag->tag }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div> -->
			<div class="col-md-12">
				<div class="form-group">
					<label for="summernote">Image Caption</label>
					<textarea id="caption" name="caption" class="form-control">
						@if(isset($image))
							{{ $image->caption }}
						@endif
					</textarea>
				</div>
			</div>

			<div class="col-md-12">
				<button type="submit"style="background-color:white;color:black;border-color:black;"  class="btn btn-primary btn-raised col-md-12 col-xs-12">Upload Image</button>
			</div>
		</form>
	</div><br><br><br>
@endsection
