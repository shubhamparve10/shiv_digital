@extends('layouts.app')
 

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('content')
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif

	<div class="container" style="padding-top:30px;">
					<div class="page-header row">
		<a class="btn btn-primary btn-lg " style="font-size:20px;background-color:white;color:black;border-color:black;" href="{{ url('admin/') }}" >Back</a>

						<!-- <a  class="btn btn-primary btn-lg col-md-4 col-sm-4 col-xs-4 pull-right" style="background-color:white;color:black;border-color:black;" href="{{ url('admin/images/create/') }}">New Images</a> -->
						<br><br> <h3 class="col-md-8 col-sm-8 col-xs-8">Manage Feedbacks</h3>
			</div>
		</div>

	<div class="panel panel-default container">
		<div class="panel-body row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xm-12">
				<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th> Id</th>
							<th> Name</th>
							<th> Contact No</th>
							<th> Email</th>
							<th> Rating</th>
							<th> Feedback</th>
						</tr>
					</thead>

					<tbody>
						@if(isset($feedbacks))
							@foreach($feedbacks as $feedback)
								<tr>
									<td>{{ $feedback->id }}</td>
									<td>{{ $feedback->name }}</td>
									<td>{{ $feedback->cnumber }}</td>
									<td>{{ $feedback->email }}</td>
									<td>
										@for ($i = 0; $i < $feedback->exp ; $i++)
        							<i class="fa fa-star fa-4px" style="color:orange;"></i>
    								@endfor
										</td>
									<td>{{ $feedback->caption }}</td><td>
										<div class="btn-group" role="group">
								 			<a class="btn btn-danger" href="{{ url('/admin/feedbacks/delete/'.$feedback->id) }}">Delete</a>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
			</div>
		</div>
	</div>
	
@endsection
